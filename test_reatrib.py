import unittest
from model.database import Reatribution
import datetime


class Test(unittest.TestCase):
    # test overlap
    def test_overlap_all_start_null(self):
        reatrib1 = Reatribution(
            name = 'reatrib1',
            date_start = None,
            date_end = datetime.date(2014, 12, 31),
            start_element = 10,
            end_element = 11,
            source = None,
        )
        reatrib2 = Reatribution(
            name = 'reatrib2',
            date_start = None,
            date_end = datetime.date(2016, 12, 31),
            start_element = 10,
            end_element = 14,
            source = None,
        )
        self.assertTrue(reatrib1.overlap_date(reatrib2))
        self.assertTrue(reatrib2.overlap_date(reatrib1))

    def test_overlap_all_end_null(self):
        reatrib1 = Reatribution(
            name = 'reatrib1',
            date_start = datetime.date(2014, 12, 31),
            date_end = None,
            start_element = 10,
            end_element = 11,
            source = None,
        )
        reatrib2 = Reatribution(
            name = 'reatrib2',
            date_start = datetime.date(2011, 12, 31),
            date_end = None,
            start_element = 10,
            end_element = 14,
            source = None,
        )
        self.assertTrue(reatrib1.overlap_date(reatrib2))
        self.assertTrue(reatrib2.overlap_date(reatrib1))


if __name__ == '__main__':
    unittest.main()
