.. Halto documentation master file, created by
   sphinx-quickstart on Wed Apr  5 09:43:56 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Halto's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   func
   db
   gui
