Organisation des données
########################

La base de données est séparé en deux grande partie. La partie import Hal qui représente
l'état des structure dans Hal et la partie générale. La partie générale contient
l'hstorique des structure et les réatributions.

Générale
********

.. image:: ../../img/db/db_general.png

Historique
==========

.. image:: ../../img/db/element.png

Elément
-------

Un élément correspond à une fiche structure Aurehal.

Pour chaque élément, on peut modifier les dates, supprimer l’élément de la ligne ou voir l’historique
des dates d’un élément dans Halto.

Ligne
-----

Une ligne correspond à une suite chronologique d’un ou plusieurs éléments.

Réatributions
=============

.. image:: ../../img/db/reattrib.png

Les réatributions sont générées à partir le l'historique. Elle permette pour chaques périodes de
temps de lister les structures non valide et leurs structures correspondantes sur cette pérides.

Ambiguité
---------

Si un même élément sur une période de temps données à plusieurs élement correspondant, alors
une ambiguité est automatiquement ajouter au moment du calcule.

Import Hal
**********

.. image:: ../../img/db/structurhal.png

Structure et donnée issue de Hal.
