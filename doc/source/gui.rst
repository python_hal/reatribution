Interface utilisateur
#####################

Onglets
*******

Base de données
================

.. image:: images/screen_onglet_base_de_donnees_line.png

.. image:: images/screen_onglet_base_de_donnees_element.png

Réatribution
=============

.. image:: images/screen_onglet_reatrib.png

La mise à jour des réatributions permet de mettre à jour les date des
réatributions aillant changer et d'ajouter des réattributions pour
les nouveaux éléments.

Le recalcule permet de supprimer des réattributions lié à des éléments
qui ont été supprimer.

Publication en erreur
=====================

Import Hal
==========

Menu
****

Fichier
=======

Ovrir une base de données
-------------------------

Créer une base de données
-------------------------

Créer fichier xlsx
-------------------

Permet de créé le fichier Excel pour le CCSD.

Vérification
=============

Aide
====
