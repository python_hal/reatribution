import json

import pathlib


class Instance(object):
    """ représente une instance
    """
    def __init__(self, id_: str, name: str, code: str, url: str, deprecated: str=True, **other) -> None:
        self._id = int(id_)
        self._code = code
        self._name = name
        self._url = url
        self._deprecated = bool(deprecated)

    @property
    def id(self)-> int:
        return self._id

    @property
    def code(self)-> str:
        return self._code

    @property
    def name(self)-> str:
        return self._name

    @property
    def url(self)-> str:
        return self._url

    @property
    def deprecated(self)-> bool:
        return self._deprecated


class Instances(object):
    """ représente la list des instances.
    """
    API_URL = 'https://api.archives-ouvertes.fr/ref/instance'

    def __init__(self, filename: str=None) -> None:
        self._instances = {}
        self._code2id = {}
        if filename is None:
            filename = str(pathlib.Path(__file__).parent / 'data/instance/list.json')
        with open(filename, 'r') as instance_file:
            data = json.load(instance_file)
            for instance_data in data['response']['docs']:
                instance_data['id_'] = instance_data['id']
                self._instances[instance_data['id']] = Instance(**instance_data)
                self._code2id[instance_data['code']] = instance_data['id']

    def __getitem__(self, id_or_code) -> Instance:
        try:
            id_ = int(id_or_code)
            id_ = str(id_or_code)
        except ValueError:
            try:
                id_ = self._code2id[id_or_code]
            except KeyError:
                return None
        try:
            return self._instances[id_]
        except KeyError:
            return None

    @property
    def instances_sorted(self):
        instances = self.instances
        instances.sort(key=lambda inst: inst.name)
        return instances

    @property
    def instances(self):
        return [instance for instance in self._instances.values()]

    @property
    def non_deprecated_instances(self):
        return [instance for instance in self._instances.values() if instance.deprecated]

    def __iter__(self):
        self._curent_id = 0
        self._tmp_instances = self.instances
        return self

    def __next__(self)-> Instance:
        if self._curent_id < len(self._tmp_instances) - 1:
            self._curent_id += 1
            return self._tmp_instances[self._curent_id]
        else:
            del self._curent_id
            del self._tmp_instances
            raise StopIteration

if __name__ == '__main__':
    instances = Instances()
    for instance in instances:
        print('nom : ' + instance.name)
        print('code : ' + instance.code)
        print('id : ' + str(instance.id))
        print('url : ' + instance.url)
        print('deprecated : ' + str(instance.deprecated))
    print(instances['saga'].name)
    print(instances[100].name)
    print(len(instances.non_deprecated_instances))
    print(len(instances.instances))
