import requests
import json

from requests.exceptions import ConnectionError

import pathlib
data_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'data')

REF_DETAIL = {
    'search': {
        'url': 'https://api.archives-ouvertes.fr/search/',
        'data_file': str(data_dir / 'search' / 'fields_description.json')
    },
    'crac': {
        'url': 'https://api.archives-ouvertes.fr/crac/hal',
        'data_file': str(data_dir / 'search' / 'fields_description.json')
    },
    'ref/structure': {
        'url': 'https://api.archives-ouvertes.fr/ref/structure',
        'data_file': str(data_dir / 'structure' / 'fields_description.json')
    },
    'ref/metadatalist': {
        'url': 'https://api.archives-ouvertes.fr/ref/metadatalist',
        'data_file': str(data_dir / 'metadatalist' / 'fields_description.json')
    },
    'ref/journal': {
        'url': 'https://api.archives-ouvertes.fr/ref/journal',
        'data_file': str(data_dir / 'journal' / 'fields_description.json')
    },
    'ref/europeanproject': {
        'url': 'https://api.archives-ouvertes.fr/ref/europeanproject',
        'data_file': str(data_dir / 'europeanproject' / 'fields_description.json')
    },
    'ref/domain': {
        'url': 'https://api.archives-ouvertes.fr/ref/domain',
        'data_file': str(data_dir / 'domain' / 'fields_description.json')
    },
    'ref/author': {
        'url': 'https://api.archives-ouvertes.fr/ref/author',
        'data_file': str(data_dir / 'author' / 'fields_description.json')
    },
    'ref/anrproject': {
        'url': 'https://api.archives-ouvertes.fr/ref/anrproject',
        'data_file': str(data_dir / 'anrproject' / 'fields_description.json')
    },
    'test_file_search': {
        'url': 'https://api.archives-ouvertes.fr/search/',
        'data_file': str(data_dir.parent / 'test.json')
    },
}


class HalDoc(object):
    def __init__(self, data, fields: dict):
        self._data = data
        self._fields = fields

    def to_dict(self):
        return {item[0]: item[1] for item in self.items()}

    def has_key(self, k):
        return k in self.keys()

    def keys(self):
        keys = []
        for current_key in self._data.keys():
            if current_key in self._fields['revers']:
                current_key = self._fields['revers'][current_key]
            keys.append(current_key)
        return keys

    def values(self):
        return [self[key] for key in self.keys()]

    def items(self):
        return ((key, self[key]) for key in self.keys())

    def __getitem__(self, key):
        if key in self._fields:
            current_name = self._fields[key]['currentName']
            if 'valuesTranslator' in self._fields[key] and self._data[current_name] in self._fields[key]['valuesTranslator'].keys():
                return self._fields[key]['valuesTranslator'][self._data[current_name]]
            key = current_name
        return self._data[key]

    def __repr__(self) -> str:
        return str(self.to_dict())

class Responses(object):
    def __init__(self, data_result: requests.Response, fields: dict, get_next_cursor=None) -> None:
        self._data_result = data_result
        self._curent_id = 0
        self._num_found = 0
        self._get_next_cursor = get_next_cursor
        self._reponses = []
        self._fields = fields
        if data_result is not None:
            try:
                json_data = data_result.json()
            except requests.exceptions.JSONDecodeError:
                json_data = {}
            has_respose = 'response' in json_data.keys()
            if has_respose:
                self._reponses =  json_data['response']['docs']
                self._num_found = json_data['response']['numFound']

    @property
    def num_found(self):
        return self._num_found

    def __len__(self):
        return len(self._reponses)

    def __iter__(self):
        self._curent_id = 0
        return self

    def __next__(self):
        if self._curent_id < len(self):
            self._curent_id += 1
            return self[self._curent_id - 1]
        else:
            raise StopIteration

    def __getitem__(self, key):
        if key < len(self._reponses):
            return HalDoc(self._reponses[key], self._fields)
        else:
            if self._get_next_cursor is None or key > self._num_found:
                raise IndexError('list index out of range')
            else:
                data_result = self._get_next_cursor()
            try:
                json_data = data_result.json()
            except requests.exceptions.JSONDecodeError:
                json_data = {}
            self._reponses += json_data['response']['docs']
            return self[key]

    @property
    def num_found(self):
        return self._num_found


class HalQueryElement(object):
    def __init__(self, fields: dict) -> None:
        self._field = '*'
        self._value = '*'
        self._fields = fields

    @property
    def field(self):
        return self._fields['revers'][self._field]
    
    @field.setter
    def field(self, field):
        current_field_name = field
        if field in self._fields:
            current_field_name = self._fields[field]['currentName']
        self.field = current_field_name

    def __repr__(self) -> str:
        if self._field is None:
            return str(self._value)
        else:
            return str(self._field) + ':' + str(self._value)


class HalQueryElementTree(HalQueryElement):
    def __init__(self, fields: dict) -> None:
        super().__init__(fields)
        self._field = None
        self._value = '*'


class HalQuery(object):
    def __init__(self, ref='search', use_cursor=False) -> None:
        self._params = {
            'q': {'field': '*', 'value': '*'},
            'fq': [],
            'rows': 10,
        }
        self._response = None
        self._has_change = True
        self._url = REF_DETAIL[ref]['url']
        self._req = None
        self._use_cursor = use_cursor
        self._next_cursor = '*'
        with open(REF_DETAIL[ref]['data_file']) as f:
            field_data_json = json.load(f)
            self._fields = {
                'revers': {}
            }
            for _, field_property in field_data_json.items():
                for field_variant in field_property['variant']:
                    self._fields[field_variant['name']] = field_variant
                    if field_variant['currentName'] != field_variant['name']:
                        self._fields['revers'][field_variant['currentName']] = field_variant['name']

    def clear_filter(self):
        self._has_change = True
        self._params['fq'] = []

    def change_q(self, field, value):
        self._has_change = True
        current_field_name = field
        if field in self._fields:
            current_field_name = self._fields[field]['currentName']
        self._params['q'] = {'field': current_field_name, 'value': value}

    def change_q_field(self, field):
        self._has_change = True
        current_field_name = field
        if field in self._fields:
            current_field_name = self._fields[field]['currentName']
        self._params['q']['field'] = current_field_name

    def change_q_value(self, value):
        self._has_change = True
        self._params['q']['value'] = value

    def add_filter(self, field, value):
        self._has_change = True
        current_field_name = field
        if field in self._fields:
            current_field_name = self._fields[field]['currentName']
        self._params['fq'].append(current_field_name + ':' + value)

    def add_returned_field(self, field):
        self._has_change = True
        current_field_name = field
        if field in self._fields:
            current_field_name = self._fields[field]['currentName']
        if 'fl' in self._params:
            self._params['fl'] += ',' + current_field_name
        else:
            self._params['fl'] = current_field_name

    @property
    def rows(self):
        return self._params['rows']

    @rows.setter
    def rows(self, rows: int):
        self._has_change = True
        self._params['rows'] = rows

    @property
    def use_cursor(self):
        return self._use_cursor

    @use_cursor.setter
    def use_cursor(self, use_cursor: bool):
        self._has_change = True
        self._use_cursor = use_cursor

    @property
    def responses(self):
        if self._response is None or self._has_change:
            get_next_cursor = self._send_query if self._use_cursor else None
            data_result =self._send_query()
            self._response = Responses(data_result, self._fields, get_next_cursor)
            self._has_change = False
        return self._response

    def _send_query(self):
        if self._use_cursor and self._next_cursor == None:
            return
        self._req = requests.Request('GET', self._url, params=self.params)
        s = requests.Session()
        try:
            data_result = s.send(self._req.prepare())
        except:
            data_result = None
        if self._use_cursor:
            try:
                json_data = data_result.json()
            except requests.exceptions.JSONDecodeError:
                json_data = {'nextCursorMark':'*'}
            if self._next_cursor == json_data['nextCursorMark']:
                self._next_cursor = None
            self._next_cursor = json_data['nextCursorMark']
        return data_result

    @property
    def full_url(self):
        return self._req.prepare().path_url if self._req is not None else ''

    @property
    def params(self):
        params = []
        if self._use_cursor:
            params.append(('cursorMark', self._next_cursor))
            params.append(('sort', 'docid asc'))
        for param in self._params.items():
            if param[0] == 'fq':
                for fq in param[1]:
                    params.append(('fq', fq))
            elif param[0] == 'q':
                if param[1]['field'] == '*':
                    params.append(('q', param[1]['value']))
                else:
                    params.append(('q', param[1]['field'] + ':' + param[1]['value']))
            else:
                params.append(tuple(param))
        return tuple(params)

if __name__ == '__main__':
    q = HalQuery('test_file_search')
    q.add_returned_field('test')
    q.add_returned_field('docid')
    print(q.params)
    q.rows = 2
    print(q.responses.num_found)
    for response in q.responses:
        print(response)
        print(response.keys())
        print(response['test'])
        print(response['docid'])
        for item in response.items():
            print('item => ')
            print(item)
    q.add_filter('docid', "3988240")
    print(q.responses.num_found)
    print(q.responses[0])
    print(q.responses[0].has_key('test'))
    print(q.responses[0].has_key('title_s'))
    fq = HalQuery()
    fq.add_filter('rgrpInstStructId_i', '1042703')
    fq.add_filter('fulltext_t', 'marque')
    print(fq.responses.num_found)
    print(fq.full_url)
    print(fq.responses[0])
    qc = HalQuery(use_cursor=True)
    qc.add_filter('rgrpInstStructId_i', '1042703')
    qc.rows = 2
    reponses = qc.responses
    print(reponses[10])
    print(reponses[5])
