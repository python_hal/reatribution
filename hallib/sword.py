import requests
from requests.auth import HTTPBasicAuth
from lxml import etree
from io import StringIO
import keyring


class TEIFile(object):
    """
    Fichier TEI
    """
    NS = {"tei": "http://www.tei-c.org/ns/1.0"}
    parser = etree.XMLParser(remove_blank_text=True)

    def __init__(self, filename=None, content=None) -> None:
        self.content = '' if content is None else content
        if filename is not None:
            self.open_from_file(filename)

    @property
    def content(self):
        return self._tei_dom.getroot()

    @property
    def content_str(self):
        return etree.tostring(self.content, encoding='unicode', pretty_print=True)

    def del_list_org(self):
        for struct_list in self._tei_dom.xpath( "//tei:listOrg", namespaces=TEIFile.NS):
            parent = struct_list.getparent()
            struct_list.getparent().remove(struct_list)
            if not len(list(parent)):
                parent.getparent().remove(parent)

    def replace_struct_affiliat(self, old_struct, new_struct):
        modified = False
        for bad in self._tei_dom.xpath( "//tei:affiliation[@ref='#struct-" + old_struct + "']", namespaces=TEIFile.NS):
            modified = True
            parent = bad.getparent()
            if len(parent.xpath( "//tei:affiliation[@ref='#struct-" + new_struct + "']", namespaces=TEIFile.NS)) > 1:
                parent.remove(bad)
                continue
            bad.attrib['ref'] = '#struct-' + new_struct
        return modified

    @content.setter
    def content(self, value):
        self._content = value
        if self._content != '':
            self._tei_dom = etree.parse(StringIO(self._content), TEIFile.parser)

    def open_from_file(self, filename):
        with open(filename, "r", encoding="utf-8") as open_file:
            self.content = open_file.read()

    def save_to_file(self, filename):
        with open(filename, "w", encoding="utf-8") as save_file:
            save_file.write(self._content)


class Sword(object):
    """
    API Sword
    """
    SWORD_URL = "https://api.archives-ouvertes.fr/sword/"

    def __init__(self, instence) -> None:
        self._instance = instence

    def send_TEI(self, TEI_file: TEIFile, halId_s, login, password):
        headers = {
            'Content-Type': 'text/xml',
            'Packaging': 'http://purl.org/net/sword-types/AOfr',
        }
        return requests.put(
            Sword.SWORD_URL + self._instance + '/' + halId_s,
            data=TEI_file.content_str.encode('utf-8'),
            headers=headers,
            auth=HTTPBasicAuth(login, password)
        )


if __name__ == '__main__':
    import pathlib
    filename = pathlib.Path(__file__).parent.parent / 'backup/1001147_new.xml'
    TEI_file = TEIFile(filename=filename)
    print(TEI_file.content)
