from PyQt6.QtWidgets import QApplication
from view.element_widget import ElementWidget
from model.database import Element, model_list, db
import sys


if __name__ == '__main__':
    db.init('reattribution.db')
    db.connect()
    db.create_tables(model_list)
    app = QApplication(sys.argv)
    app.quitOnLastWindowClosed = True
    element_widget = ElementWidget(Element())
    element_widget.show()
    sys.exit(app.exec())
