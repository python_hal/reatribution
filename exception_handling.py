import logging
import inspect
import sys
import traceback


class ExceptionHandler(object):
    def __init__(self) -> None:
        self._optional_handlers = []

    def add_handler(self, new_handler):
        self._optional_handlers.append(new_handler)

    def exit_program(self):
        sys.exit()

    def handle_exception(self, exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return
        logging.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
        traceback.print_tb(exc_traceback)
        args = [exc_type, exc_value, exc_traceback]
        for handler in self._optional_handlers:
            sig = inspect.signature(handler)
            nb_pos_args = [param.default == param.empty for param in sig.parameters.values()].count(True)
            while len(args) > nb_pos_args:
                args.pop()
            handler(*args)