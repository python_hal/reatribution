# HalTo

Gestion d'un historique des structure Hal lié à un portail.

## Utilisation

installation des bibliothèques requieses :

```powershell
pip install -r requirements.txt
```

### Lancement

```powershell
python main.py
```

### Construction aide

```powershell
cd doc
make.bat
```

### Création d'un setup d'installation

Le fichier msi créé peut être installé sans les droit administrateur.
La bibliothèque cx_freeze est utilisé pour créé le fichier msi.

```powershell
python.exe .\setup.py bdist_msi  
```

## Hallib

Hallib utilise les données de [Hallib-php](https://gricad-gitlab.univ-grenoble-alpes.fr/picotg/hallib-php) pour générée et executé les requête Hal.

### instance.py

Liste est outil pour chercher les instances.

```python
# lister toutes les instances du fichier instance et afficher toutes les infos
instances = Instances()
for instance in instances:
    print('nom : ' + instance.name)
    print('code : ' + instance.code)
    print('id : ' + str(instance.id))
    print('url : ' + instance.url)
    print('deprecated : ' + str(instance.deprecated))

# Accédé à une instance par son code
insatcne_uga = instances['saga']
# ou par son id (Hal)
insatcne_uga = instances[10]
```

### query.py

Construction et execution des requête Hal.

Les fichier de données de [Hallib-php](https://gricad-gitlab.univ-grenoble-alpes.fr/picotg/hallib-php) permette de prendre en compte les changement de nom de champs ou de valeur de la même façon que pour [Hallib-php](https://gricad-gitlab.univ-grenoble-alpes.fr/picotg/hallib-php).

```python
# création et execution d'une requête dans Hal
# pour une requête recherche
q = HalQuery('search')
# ajout de champs de retour
q.add_returned_field('docid')
q.add_returned_field('title_s')
# choix du nombre de retour
q.rows = 2
# affichage nobmre de résultats
print(q.responses.num_found)
# affichage de chaque rsultat
for response in q.responses:
    print(response)
    print(response.keys())
    print(response['docid'])
    # lister tous les champs de retour et leurs valeurs
    for item in response.items():
        print(item)
```

### sword.py

API de construction et d'execution des requête [SWORD](https://api.archives-ouvertes.fr/docs/sword) pour la modification d'un document Hal.

Exemple de code pour le changement d'affiliation :

```python
from hallib.sword import Sword, TEIFile
sword = Sword('saga')
login = 'login'
password = 'pass'
tei_file = TEIFile(filename='tei.xml')
# suppression liste des organisation (rajouter par Hal automatiquement)
tei_file.del_list_org()
# change assienne affiliation par la nouvelle
tei_file.replace_struct_affiliat('old_struct', 'new_struct')
# envoi send TEI file au server Hal
sword.send_TEI(tei_file, 'halId_s', login, password)
```

## base de données

La base de données est séparer en principalement 2 grosses parties. La partie "général" content tout le contenu de la base surlequel on travail. La partie structure Hal contient les données issu de Hal.

## Import de racine

Les racine peuvent être importé sous forme d'un tableau d'id de structure Hal dans un fichier JSON.

exemple :
```json
[2381, 446819, 517]
```

### général

Gestion des données de travail.

![Shéma de la partie général](img/db/db_general.png)

#### Réatributions

Partie gestion des réatributions de la partie générale

![Shéma des réatributions](img/db/reattrib.png)

#### Élements

Partie gestion des élements de la partie générale

![Shéma des réatributions](img/db/element.png)

### structure Hal

![Shéma de la partie structure Hal](img/db/structurhal.png)

### Erreur

![Shéma de la gestion des erreurs](img/db/error.png)

## sources des icones

- https://openclipart.org/detail/174484/red-cross
- https://openclipart.org/detail/188650
- https://openclipart.org/detail/307425/warning-icon
