from cx_Freeze import setup, Executable
import pathlib
import sys

app_dir = pathlib.Path(__file__).parent.resolve()
icon_path = str(app_dir / 'img/halto.ico')
search_path = str(app_dir / 'img/search.ico')
cancel_path = str(app_dir / 'img/cancel.ico')
warning_path = str(app_dir / 'img/warning.ico')
GraphViz_path = str(app_dir.parent / 'Graphviz')
backup_path = str(app_dir / 'backup/create.txt')
doc_path = str(app_dir / 'doc/build/html')

build_exe_options = {
    "packages": ["os", "keyring", 'keyring.backends', 'win32timezone', 'win32'],
    "excludes": ["tkinter"],
    "include_files": [
        (icon_path, 'Lib/img/halto.ico'),
        (search_path, 'Lib/img/search.ico'),
        (cancel_path, 'Lib/img/cancel.ico'),
        (warning_path, 'Lib/img/warning.ico'),
        (backup_path, 'Lib/backup/create.txt'),
        GraphViz_path, (doc_path, 'Lib/doc/build/html/')
    ]
}

base = "Win32GUI" if sys.platform == "win32" else None
# base="Console"

directory_table = [
    ("ProgramMenuFolder", "TARGETDIR", "."),
    ("HaltoMenu", "ProgramMenuFolder", "HALTO|Halto"),
]

msi_data = {
    "Directory": directory_table,
    "ProgId": [
        ("Prog.Id", None, None, "This is a description", "IconId", None),
    ],
    "Icon": [
        ("IconId", icon_path),
    ],
}

bdist_msi_options = {
    "data": msi_data,
    "upgrade_code": "{8a8e52da-c73d-4cad-adbb-0139ff32c622}",
}

setup(
    name="Halto",
    version="0.1.0-beta12",
    description="Application graphique pour la gestion des historiques de structure dans Aurehal.",
    options={
        "build_exe": build_exe_options,
        "bdist_msi": bdist_msi_options,
    },
    executables=[Executable("main.py",
                            base=base,
                            shortcut_name="Halto (AurehaL hisTOrique)",
                            shortcut_dir='ProgramMenuFolder',
                            icon = icon_path)]
)
