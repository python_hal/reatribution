from PyQt6.QtWidgets import QApplication, QMessageBox
from view.main_window import MainWindow, app_dir
import sys
import logging
from exception_handling import ExceptionHandler

logging.basicConfig(filename=str(app_dir / 'error.log'), level=logging.WARNING)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.quitOnLastWindowClosed = True
    exception_handler = ExceptionHandler()
    sys.excepthook = exception_handler.handle_exception
    window = MainWindow(exception_handler)
    window.showMaximized()
    sys.exit(app.exec())
