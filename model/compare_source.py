from model.database import *


class CompareSourceForLine(object):
    def __init__(self, line:Line, sources:list=[]) -> None:
        self._reatribution = None
        self._line = line
        self.build_reatribution()
        if len(sources) < 2:
            self._sources = list(self._reatribution.keys())
        else:
            self._sources = sources

    def build_reatribution(self):
        if self._reatribution is None:
            self._reatribution = {}
            for line_element in LineElement.select().where(LineElement.line == self._line):
                element = line_element.element
                for reatribution in Reatribution.select().where((Reatribution.start_element == element.hal_id) | (Reatribution.end_element == element.hal_id)):
                    if reatribution.source not in self._reatribution.keys():
                        self._reatribution[reatribution.source] = []
                    if reatribution not in self._reatribution[reatribution.source]:
                        self._reatribution[reatribution.source].append(reatribution)

    def comp_source(self, source1, source2, check_null):
        for source in [source1, source2]:
            if source not in self._reatribution.keys():
                return False
        if len(self._reatribution[source1]) != len(self._reatribution[source2]):
            return False
        for reatribation1 in self._reatribution[source1]:
            not_found = True
            for reatribation2 in self._reatribution[source2]:
                if reatribation1.same_elements(reatribation2):
                    if reatribation1.same_dates(reatribation2, check_null):
                        not_found = False
            if not_found:
                return False
        return True

    def same_reatrib(self, check_null=False) -> bool:
        if len(self._sources) < 2:
            return False
        first_source = self._sources[0]
        for other_source in self._sources[1:]:
            if not self.comp_source(first_source, other_source, check_null):
                return False
        return True

    @property
    def diff_category(self) -> str:
        if len(self._reatribution.keys()) == 0:
            return 'none'
        if self.same_reatrib():
            return 'same'
        if self.same_reatrib(True):
            return 'null or not'
        return 'diff'
