from enum import Enum
import configparser
from hallib.instance import Instances


import pathlib
current_dir = pathlib.Path(__file__).parent.resolve()


class AutoBackupOption(Enum):
    ONE_PER_DAY = 1
    EVERY_OPEN = 2
    No = 3

    @property
    def fr_label(self) -> str:
        if self == AutoBackupOption.ONE_PER_DAY:
            return 'Une sauvegarde par jour'
        elif self == AutoBackupOption.EVERY_OPEN:
            return 'Une sauvegarde à chaque ouverture de base de donnée'
        else:
            return 'Pas de sauvegarde automatique'


class Options(object):
    def __init__(self):
        self._config = configparser.ConfigParser()
        self._config.read('config.ini')
        self._instances = Instances()

        # read config
        if 'general' not in self._config:
            self._config['general'] = {
                'start_year': 2016,
                'instance': '',
                'last_database': '',
                'auto_backup': 1
            }

    @property
    def start_year(self):
        return self._config['general']['start_year'] if 'general' in self._config and 'start_year' in self._config['general'] else ''

    @start_year.setter
    def start_year(self, start_year):
        self._config['general']['start_year'] = start_year

    @property
    def autobackup(self)-> AutoBackupOption:
        return AutoBackupOption(int(self._config['general']['auto_backup'] if 'general' in self._config and 'auto_backup' in self._config['general'] else 1))

    @autobackup.setter
    def autobackup(self, new_value):
        if isinstance(new_value, AutoBackupOption):
            new_value = new_value.value
        self._config['general']['auto_backup'] = str(new_value)

    @property
    def last_database(self):
        return self._config['general']['last_database'] if 'general' in self._config and 'last_database' in self._config['general'] else ''

    @last_database.setter
    def last_database(self, database):
        self._config['general']['last_database'] = database

    @property
    def instances(self)-> Instances:
        return self._instances

    @property
    def instance(self):
        instance_id = self._config['general']['instance'] if 'general' in self._config and 'instance' in self._config['general'] else None
        if instance_id is None:
            return None
        return self._instances[instance_id]

    @instance.setter
    def instance(self, instance):
        self._config['general']['instance'] = str(self._instances[instance].id)

    @property
    def login(self):
        return self._config['logins']['login'] if 'logins' in self._config else ''

    @login.setter
    def login(self, login):
        if 'logins' not in self._config:
            self._config['logins'] = {
                'login': login,
            }
        else:
            self._config['logins']['login'] = login

    def save(self):
        with open('config.ini', 'w') as config_file:
            self._config.write(config_file)
