from openpyxl import load_workbook
import datetime
import calendar


def str_date_to_date(str_date, end=False):
    if str_date == '....':
        return None
    if str_date.count('-') == 2:
        day, month, year = str_date.split('-')
        return datetime.date(int(year), int(month), int(day))
    if str_date.count('-') == 1:
        month, year = str_date.split('-')
        if end:
            return datetime.date(int(year), int(month), calendar.monthrange(int(year), int(month))[1])
        return datetime.date(int(year), int(month), 1)
    if str_date.count('_') == 2:
        day, month, year = str_date.split('_')
        return datetime.date(int(year), int(month), int(day))
    if str_date.count('_') == 1:
        month, year = str_date.split('_')
        if end:
            return datetime.date(int(year), int(month), calendar.monthrange(int(year), int(month))[1])
        return datetime.date(int(year), int(month), 1)
    else:
        if end:
            return datetime.date(int(str_date), 12, 31)
        return datetime.date(int(str_date), 1, 1)



if __name__ == '__main__':
    from PyQt6.QtWidgets import QFileDialog, QApplication
    import pathlib
    import sys
    app = QApplication(sys.argv)
    fname = QFileDialog.getOpenFileName(None, 'Open file', str(pathlib.Path.home()))[0]
    from database import *
    db.init('reattribution.db')
    db.connect()
    db.create_tables(model_list)
    wb = load_workbook(fname)
    for sheet in wb:
        try:
            start_date, end_date = sheet.title.split('-')
        except ValueError:
            start_date, end_date = sheet.title.split('_')
        start_date, end_date = (str_date_to_date(start_date, False), str_date_to_date(end_date, True))
        for i, row in enumerate(sheet.iter_rows()):
            if i != 0:
                try:
                    start_line = Line.select().where(Line.hal_id == int(row[1].value)).get()
                except TypeError:
                    continue
                except :
                    start_line = Line(hal_id =int(row[1].value))
                    start_line.save()
                try:
                    end_line = Line.select().where(Line.hal_id == int(row[2].value)).get()
                except TypeError:
                    continue
                except :
                    end_line = Line(hal_id =int(row[2].value))
                    end_line.save()
                r = Reatribution(name=row[0].value, date_start=start_date, date_end=end_date, start_struct=start_line, end_struct=end_line)
                r.save()
    fname = QFileDialog.getOpenFileName(None, 'Open file', str(pathlib.Path.home()))[0]
    wb = load_workbook(fname)
    sheet = wb['StructurHal 20210331']
    for i, row in enumerate(sheet.iter_rows()):
        if i == 0: continue
        try:
            line = Line.select().where(Line.hal_id == int(row[0].value)).get()
        except ValueError:
            continue
        except:
            line = Line(hal_id=int(row[0].value))
            line.save()
        line.label=row[2].value if row[2].value!=None else ''
        line.acronym=row[1].value if row[1].value!=None else ''
        line.type=row[4].value if row[4].value!=None else ''
        line.save()
    sys.exit(0)
