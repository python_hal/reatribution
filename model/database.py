from peewee import *
from calendar import monthrange
from PyQt6.QtCore import QObject
import typing
import re
import sys
import pathlib


# List des types de structure Hal
STRUCTURE_TYPE = (
        ('department', 'department'),
        ('institution', 'institution'),
        ('laboratory', 'laboratory'),
        ('regrouplaboratory', 'regrouplaboratory'),
        ('researchteam', 'researchteam'),
    )


def date_to_string(date_, end_=False):
    """
    transforme une date en string attendu dans le fichier du CCSD
    """
    if date_ is None:
        return '....'
    else:
        if end_:
            invisible_day = monthrange(date_.year, date_.month)[1]
            invsible_month = 12
        else:
            invisible_day = 1
            invsible_month = 1
        if date_.day == invisible_day:
            if date_.month == invsible_month:
                return date_.strftime('%Y')
            else:
                return date_.strftime('%m_%Y')
        else:
            return date_.strftime('%d_%m_%Y')


# base de donner
db = SqliteDatabase(None)


class DBChangeNotifier():
    """
    class de l'objet charger de notifier les modification de la base de données.
    """

    def __init__(self):
        # liste des obserer
        self._observers = {'new': {}, 'update': {}, 'before_saving': {}, 'delete': {}}

    def add_all_events(self, observer):
        """
        Ajoute toute les méthodes dont le nom correspond à un événement à la liste des événements.
        """
        all_added_observer = set()
        for element_name in dir(observer):
            for event in ['new', 'update', 'before_saving']:
                if element_name.startswith('db_' + event):
                    classname = self.register_obeserver(event, getattr(observer, element_name))
                    all_added_observer.add((event, getattr(observer, element_name), classname))
            if element_name.startswith('db_delete'):
                delete_onserver = getattr(observer, element_name)
                doc_string = delete_onserver.__doc__
                matching = re.search("db_delete_event:.*;", doc_string)
                if matching is not None:
                    classname = matching.group()[len('db_delete_event:'):-1].strip()
                    self.register_obeserver('delete', getattr(observer, element_name), classname)
                    all_added_observer.add((event, getattr(observer, element_name), classname))
        if isinstance(observer, QObject):
            observer.destroyed.connect(self._all_event_remover(all_added_observer))

    def _all_event_remover(self, all_added_observer):
        """
        enleve tous les événement d'un QObject à ça suppression pour éviter les
        bugs d'objet déjà détruis.
        """
        def remover():
            for added_observer in all_added_observer:
                self.unregister_obeserver(*added_observer)
        return remover

    def unregister_obeserver(self, event, observer, classname: str):
        """
        désenregistre un observer d'un élément
        """
        if classname in self._observers[event]:
            try:
                self._observers[event][classname].remove(observer)
            except ValueError:
                pass

    def register_obeserver(self, event, observer, classname: str|None=None):
        """
        désenregistre un observer à un élément
        """
        if classname is None:
            classname = list(typing.get_type_hints(observer).values())[0].__name__
        if classname not in self._observers[event]:
            self._observers[event][classname] = []
        if observer not in self._observers[event][classname]:
            self._observers[event][classname].append(observer)
        return classname

    def emit(self, new_element, event: str):
        """
        émet un événment
        """
        classname = new_element.__class__.__name__
        if classname in self._observers[event]:
            for observer in self._observers[event][classname]:
                observer(new_element)

    def emit_delete(self, classname, deleted_element_id):
        """
        émet un événment de supression
        """
        if classname in self._observers['delete']:
            for observer in self._observers['delete'][classname]:
                observer(deleted_element_id)


db_change_notifier = DBChangeNotifier()


class BaseModel(Model):
    """
    class model de base
    """
    class Meta:
        database = db

    def save(self, *args, **kwargs):
        """
        sauvegarde l'objet et envoi les événements
        """
        id_before = self.id
        db_change_notifier.emit(self, 'before_saving')
        super_res = super().save(*args, **kwargs)
        if id_before is None:
            db_change_notifier.emit(self, 'new')
        else:
            db_change_notifier.emit(self, 'update')
        return super_res


def date_before_or_none(date1, none_is_end1, date2, none_is_end2):
    """
    verifi si une date et avant ou en cas de null cossidére comme allant à
    l'infini ou à -l'infini suivant none_is_end2
    """
    if date1 is None:
        return not none_is_end1
    if date2 is None:
        return none_is_end2
    return date1 <= date2


class Element(BaseModel):
    """
    element d'une ligne
    """
    hal_id = IntegerField(unique=True)
    date_start = DateField(formats=['%d_%m_%Y', '%m_%Y', '%Y', '%Y-%m-%d'], null=True)
    unknow_start = BooleanField(default=False)
    date_end = DateField(formats=['%d_%m_%Y', '%Y-%m-%d'], null=True)
    unknow_end = BooleanField(default=False)
    recalculate_need = BooleanField(default=True)

    def modified_since(self, old_version: 'Element') -> bool:
        """
        Verifie s'il y a eu des modifier depuis la dernière version
        """
        result = False
        result = result or old_version.date_start != self.date_start
        result = result or old_version.unknow_start != self.unknow_start
        result = result or old_version.date_end != self.date_end
        result = result or old_version.unknow_end != self.unknow_end
        return result

    def end_overlap_date(self, other) -> bool:
        if self.date_start == other.date_start:
            return True
        if date_before_or_none(self.date_start, False, other.date_start, False) and date_before_or_none(other.date_start, False, self.date_end, True):
            return True
        return False

    def overlap_date(self, other) -> bool:
        return self.end_overlap_date(other) or other.end_overlap_date(self)


class Line(BaseModel):
    """
    ligne
    """
    label = CharField(default='')
    acronym = CharField(default='')
    type = CharField(default='', choices=STRUCTURE_TYPE)
    elements = ManyToManyField(Element, backref='lines')

    @property
    def last_element(self):
        return self.elements.order_by(Element.date_start, -Element.date_end)[-1]


class Reatribution(BaseModel):
    """
    réatribution
    """
    name = CharField(max_length=50)
    date_start = DateField(formats=['%d_%m_%Y', '%m_%Y', '%Y', '%Y-%m-%d'], null=True)
    date_end = DateField(formats=['%d_%m_%Y', '%Y-%m-%d'], null=True)
    start_element = IntegerField()
    end_element = IntegerField()
    periods = None

    def end_overlap_date(self, other) -> bool:
        if self.date_start == other.date_start:
            return True
        if date_before_or_none(self.date_start, False, other.date_start, False) and date_before_or_none(other.date_start, False, self.date_end, True):
            return True
        return False

    def overlap_date(self, other) -> bool:
        return self.end_overlap_date(other) or other.end_overlap_date(self)

    def value_eq(self, other) -> bool:
        if self.date_start != other.date_start:
            return False
        if self.date_end != other.date_end:
            return False
        if self.start_element != other.start_element:
            return False
        if self.end_element != other.end_element:
            return False
        return True

    def same_dates(self, other, allow_null_on_end=False) -> bool:
        if self.date_end != other.date_end:
            if allow_null_on_end:
                if len(Reatribution.select().where((Reatribution.start_element == self.end_element) |
                                                    (Reatribution.start_element == other.end_element))) == 0:
                    return False
                else:
                    if other.date_end != None and self.date_end != None:
                        return False
            else:
                return False
        if self.date_start != other.date_start:
            if allow_null_on_end:
                if len(Reatribution.select().where((Reatribution.end_element == self.start_element) |
                                                    (Reatribution.end_element == other.start_element))) == 0:
                    return False
                else:
                    if other.date_start != None and self.date_start != None:
                        return False
            else:
                return False
        return True

    def same_elements(self, other) -> bool:
        if self.start_element != other.start_element:
            return False
        if self.end_element != other.end_element:
            return False
        return True

    @property
    def period(self):
        period = date_to_string(self.date_start)
        period += '-'
        period += date_to_string(self.date_end, True)
        return period

    @classmethod
    def addPeriod(cls, period: str, data_period: tuple):
        if cls.periods is None:
            cls.periods = {}
        if period not in cls.periods:
            cls.periods[period] = data_period

    @classmethod
    def getPeriods(cls) -> dict:
        if cls.periods is None:
            try:
                for reatrib in cls.select():
                    cls.addPeriod(reatrib.period, (reatrib.date_start, reatrib.date_end))
            except InterfaceError:
                return {}
                
        return cls.periods


class Ambiguity(BaseModel):
    """
    Ambiguité avec même début et fin différente pour des dates identique.
    """
    reatributions = ManyToManyField(Reatribution, backref='ambiguities')
    start_element = ForeignKeyField(Element)
    date_start = DateField(formats=['%d_%m_%Y', '%m_%Y', '%Y', '%Y-%m-%d'], null=True)
    date_end = DateField(formats=['%d_%m_%Y', '%Y-%m-%d'], null=True)


DeferredStructureHal = DeferredForeignKey('parents')


class StructureHal(BaseModel):
    """
    line directement issu de Hal.
    """
    docid = IntegerField(unique=True)
    last_update = DateTimeField(formats=['YYYY-MM-DD HH:MM:SS'])
    acronym_s = CharField(max_length=250, null=True)
    label_s = CharField(max_length=500, null=True)
    valid_s = CharField(max_length=25, null=True)
    type_s = CharField(default='', choices=STRUCTURE_TYPE, null=True)
    address_s = CharField(max_length=500, null=True)
    parentDocid_i = CharField(max_length=500, null=True) # list séparé par une virgule
    url_s = CharField(max_length=500, null=True)
    endDate_s = DateField(formats=['%d_%m_%Y', '%m_%Y', '%Y', '%Y-%m-%d'], null=True)
    startDate_s = DateField(formats=['%d_%m_%Y', '%m_%Y', '%Y', '%Y-%m-%d'], null=True)
    aliasDocid_i = CharField(max_length=500, null=True) # list séparé par une virgule
    # Pour asavoir si les nouvelle structure doivent être ajouter ou non
    # null = pas encore décidé, false cacher, true visible
    ELEMENT_SYNC_STATUS = (
        'l\'élément n\'existe pas',
        'l\'élément a une date différent',
        'pas de différence avec',
    )
    _roots = set()

    @classmethod
    def get_roots(cls) -> set:
        return cls._roots

    @classmethod
    def recalculate_roots(cls):
        for struct in cls.select():
            if not ParentStructureHal.select().where(ParentStructureHal.child==struct).exists():
                cls._roots.add(struct.docid)

    @classmethod
    def new_element_check(cls, new_element):
        is_root = True
        if new_element.parentDocid_i is not None:
            for parent in new_element.parentDocid_i.split(','):
                if cls.select().where(cls.docid == parent).exists():
                    is_root = False
                    break
        if is_root:
            cls._roots.add(new_element.docid)
            for root_id in list(cls._roots):
                root = cls.get_or_none(cls.docid == root_id)
                if root is None:
                    cls._roots.remove(root_id)
                else:
                    if root.parentDocid_i is not None:
                        if new_element.docid in root.parentDocid_i.split(','):
                            if root.docid in cls._roots:
                                cls._roots.remove(root.docid)
        ParentStructureHal.recalculate_all()
        cls.recalculate_roots()

    @property
    def diff_element(self):
        element = Element.get_or_none(Element.hal_id == self.docid)
        if element is None:
            return self.ELEMENT_SYNC_STATUS[0]
        if (str(element.date_start) != str(self.startDate_s)) or (str(element.date_end) != str(self.endDate_s)):
            return self.ELEMENT_SYNC_STATUS[1]
        else:
            return self.ELEMENT_SYNC_STATUS[2]


db_change_notifier.register_obeserver('new', StructureHal.new_element_check, 'StructureHal')


class ParentStructureHal(BaseModel):
    parent = ForeignKeyField(StructureHal, on_delete='CASCADE')
    child = ForeignKeyField(StructureHal, on_delete='CASCADE')

    class Meta:
        indexes = (
            (('parent', 'child'), True),
        )

    @classmethod
    def get_all_parents_id(cls, child: StructureHal):
        all_parents = set()
        for child_parent in ParentStructureHal.select().where(ParentStructureHal.child == child):
            all_parents.add(child_parent.parent.docid)
            all_parents = all_parents.union(ParentStructureHal.get_all_parents_id(child_parent.parent))
        return all_parents

    @classmethod
    def recalculate_all(cls):
        cls.delete().execute()
        for structure_hal in StructureHal.select():
            if structure_hal.parentDocid_i is not None:
                for parent_docid in structure_hal.parentDocid_i.split(','):
                    parent = StructureHal.get_or_none(StructureHal.docid==parent_docid)
                    if parent:
                        new_rel = cls(parent=parent, child=structure_hal)
                        new_rel.save()


class ElementDateChange(BaseModel):
    """
    Historisation changement de date des element
    """
    element = ForeignKeyField(Element)
    original_date = DateField(formats=['%d_%m_%Y', '%m_%Y', '%Y', '%Y-%m-%d'], null=True)
    final_date = DateField(formats=['%d_%m_%Y', '%m_%Y', '%Y', '%Y-%m-%d'], null=True)
    is_start = BooleanField()


class DateElementsInconsistencies(BaseModel):
    """
    Erreur de suite de date dans une ligne.
    """
    start_hal_id = IntegerField()
    end_hal_id = IntegerField()
    line = ForeignKeyField(Line, backref='inconsistencies')
    ignore = BooleanField(default=False)


class Error(BaseModel):
    ERROR_CHOICES = (
        (0, 'structure not found'),
        (1, 'unknow error')
    )
    class_name = CharField()
    instance_id = IntegerField()
    error_valut = IntegerField(choices=ERROR_CHOICES)
    error_message = CharField(null=True)

    @property
    def object_instance(self):
        cls = getattr(sys.modules[__name__], self.class_name)
        return cls.get_or_none(cls.id==self.instance_id)

    @object_instance.setter
    def object_instance(self, value):
        self.class_name = value.__class__.__name__
        self.instance_id = value.id

    @classmethod
    def select_from_instance(cls, value):
        class_name_query = cls.class_name == value.__class__.__name__
        id_query = cls.instance_id == value.id
        return cls.select().where(class_name_query & id_query)


class CorrectionPub(BaseModel):
    docid = IntegerField()
    reatribution = ForeignKeyField(Reatribution, backref='correction_pubs')
    publication_date = DateField(formats=['%d_%m_%Y', '%m_%Y', '%Y', '%Y-%m-%d'])
    url = CharField(max_length=500, null=True)
    hal_id = CharField(max_length=20)
    status = CharField(max_length=500)
    ignored = BooleanField()

    class Meta:
        indexes = (
            (('docid', 'reatribution'), True),
        )

    @property
    def has_backup(self):
        return pathlib.Path((pathlib.Path(__file__).parent.parent / 'backup' / (str(self.docid) + '_new.xml')).resolve()).is_file()

    @property
    def backup_files(self):
        new_file = (pathlib.Path(__file__).parent.parent / 'backup' / (str(self.docid) + '_new.xml')).resolve()
        old_file = (pathlib.Path(__file__).parent.parent / 'backup' / (str(self.docid) + '_old.xml')).resolve()
        return (new_file, old_file)


class StructureTask(BaseModel):
    structure = ForeignKeyField(StructureHal, on_delete='CASCADE')
    message = CharField()
    details = TextField(default='')
    done = BooleanField(default=False)

class StructureIgnore(BaseModel):
    structure = ForeignKeyField(StructureHal, on_delete='CASCADE', backref='ignore', unique=True)
    message = CharField()
    details = TextField(default='')

model_list = [
    StructureIgnore,
    Element,
    Line,
    Reatribution,
    StructureHal,
    ElementDateChange,
    Ambiguity,
    DateElementsInconsistencies,
    ParentStructureHal,
    Error,
    StructureTask,
    CorrectionPub,
]
model_list.append(Ambiguity.reatributions.get_through_model())
model_list.append(Line.elements.get_through_model())
