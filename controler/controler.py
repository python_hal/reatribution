from openpyxl import load_workbook
import datetime
import calendar
from model.database import *
from openpyxl import Workbook
import itertools
from PyQt6.QtCore import QThread, pyqtSignal
import requests
import pathlib
from hallib.query import HalQuery
import os
import shutil
import glob
from hallib.sword import Sword, TEIFile

import keyring
from keyring.backends import Windows
keyring.set_keyring(Windows.WinVaultKeyring())


class BachupDB(object):
    def __init__(self, filename: str) -> None:
        self._filename = filename

    def __repr__(self) -> str:
        return  self._filename

    def delete(self):
        os.remove(self._filename)

    @property
    def is_valid(self):
        datetime_string = self._filename[-11:-3] if self._filename[-12] == '.' else self._filename[-20:-3]
        datetime_format = '%y-%m-%d' if self._filename[-12] == '.' else '%y-%m-%dT%H-%M-%S'
        try:
            datetime.datetime.strptime(datetime_string, datetime_format)
            return True
        except ValueError:
            return False


    @property
    def datetime(self):
        return datetime.datetime.fromtimestamp(os.path.getctime(self._filename))


class CorrectTask(object):
    STATUS = {
        'no_op': 'Aucune oppération effectuer',
        'no_mod': 'non modifier',
    }

    def __init__(self, doc_id) -> None:
        pass


class CorrectDocThread(QThread):
    """
    Thread pour la correction de l'affiliation d'un document
    """
    progress_sig = pyqtSignal(int, list)

    def __init__(self, instance, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if instance is None:
            instance='saga'
        self._query = HalQuery()
        self._query.add_returned_field('label_xml')
        self._query.rows = 1
        self._query.change_q_field('docid')
        self._sword = Sword(instance)
        self._tasks = []
        self._cancel = False

    def cancel(self):
        self._cancel = True

    def add_task(self, doc_id, old_struct_id, new_struct_id, halId_s, row):
        self._tasks.append((doc_id, old_struct_id, new_struct_id, halId_s, row))

    def _perform_task(self, task, login, password=None):
        password = password if password is not None else keyring.get_password("halto", login)
        modified = False
        self._query.change_q_value(str(task[0]))
        old_filename = str(pathlib.Path(__file__).parent.parent / 'backup' / (str(task[0]) + '_old.xml'))
        new_filename = str(pathlib.Path(__file__).parent.parent / 'backup' / (str(task[0]) + '_new.xml'))
        response = None
        for search_resp in self._query.responses:
            tei_doc = search_resp['label_xml']
            tei_file = TEIFile(content=tei_doc)
            tei_file.save_to_file(old_filename)
            tei_file.del_list_org()
            modified = tei_file.replace_struct_affiliat(task[1], task[2])
            tei_file.save_to_file(new_filename)
            if modified:
                response = self._sword.send_TEI(tei_file, task[3], login, password)
        return [task[0], task[1], task[2], response, modified, task[4]]

    def start(self, login, password=None, *args, **kwargs):
        self._login = login
        self._password = password
        super().start(*args, **kwargs)

    def run(self):
        login, self._login = self._login, ''
        password, self._password = self._password, None
        for nb_done, task in enumerate(self._tasks):
            result = self._perform_task(task, login, password)
            self.progress_sig.emit(int((nb_done/len(self._tasks)) * 100), result)
            if result[3] is not None and result[3].status_code == 401 or self._cancel:
                break


def str_date_to_date(str_date, end=False):
    if str_date == '....' or str_date == '?' or str_date == '….' or str_date == '…' or str_date == '' or str_date == '…..':
        return None
    if str_date.count('-') == 2:
        day, month, year = str_date.split('-')
        return datetime.date(int(year), int(month), int(day))
    if str_date.count('-') == 1:
        month, year = str_date.split('-')
        if end:
            return datetime.date(int(year), int(month), calendar.monthrange(int(year), int(month))[1])
        return datetime.date(int(year), int(month), 1)
    if str_date.count('_') == 2:
        day, month, year = str_date.split('_')
        return datetime.date(int(year), int(month), int(day))
    if str_date.count('_') == 1:
        month, year = str_date.split('_')
        if end:
            return datetime.date(int(year), int(month), calendar.monthrange(int(year), int(month))[1])
        return datetime.date(int(year), int(month), 1)
    else:
        if end:
            return datetime.date(int(str_date), 12, 31)
        return datetime.date(int(str_date), 1, 1)


class UpdateThread(QThread):
    """
    thread pour l'interogation de Hal sur les lines
    """
    progress_sig = pyqtSignal(int)
    new_struct_sig = pyqtSignal(dict, datetime.date)
    new_not_found = pyqtSignal(int)

    def __init__(self, element_list:set, child:bool = False, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._cancel = False
        self._nb_element = len(element_list)
        self._element_list = element_list
        self._query = HalQuery('ref/structure')
        self._query.add_returned_field('docid')
        self._query.add_returned_field('acronym_s')
        self._query.add_returned_field('label_s')
        self._query.add_returned_field('valid_s')
        self._query.add_returned_field('type_s')
        self._query.add_returned_field('address_s')
        self._query.add_returned_field('url_s')
        self._query.add_returned_field('endDate_s')
        self._query.add_returned_field('startDate_s')
        self._query.add_returned_field('parentDocid_i')
        if child:
            self._query.change_q_field('parentDocid_i')
            self._child = True
            self._query.rows = 10000
        else:
            self._query.change_q_field('docid')
            self._child = False
            self._query.rows = 1

    def cancel(self):
        self._cancel = True

    def _get_info_structure(self, docid) -> None:
        q = str(docid)
        if self._child:
            q += ' AND valid_s:(VALID OR OLD)'
        self._query.change_q_value(q)
        last_update = datetime.datetime.now()
        for doc in self._query.responses:
            self.new_struct_sig.emit(doc.to_dict(), last_update)
        if len(self._query.responses) == 0 and not self._child:
            self.new_not_found.emit(docid)
        percent_done = int(100 * (1 - len(self._element_list) / self._nb_element))
        self.progress_sig.emit(percent_done)

    def run(self):
        while len(self._element_list) > 0 :
            if self._cancel:
                break
            docid = self._element_list.pop()
            self._get_info_structure(docid)


class ErrorFindingThread(QThread):
    """
    Thread pour la recherche d'erreur
    """
    progress_sig = pyqtSignal(int)
    new_element = pyqtSignal(list)

    def __init__(self, finding_type, start_date: str='2016', *args, **kwargs) -> None:
        """
        finding_type :
        - time_end : recherche publication après la fin d'une line
        - ambiguity : à partir des reatribution ambigue
        - find_auto_correction : find auto correction
        """
        super().__init__(*args, **kwargs)
        self._cancel = False
        self._finding_type = finding_type
        self._start_date = datetime.date(year=int(start_date), month=1, day=1)
        self._query = HalQuery()
        self._query.add_returned_field('publicationDateD_i')
        self._query.add_returned_field('publicationDateM_i')
        self._query.add_returned_field('publicationDateY_i')
        self._query.add_returned_field('docid')
        self._query.add_returned_field('uri_s')
        self._query.add_returned_field('halId_s')
        self._query.rows = 10000

    def cancel(self):
        self._cancel = True

    def find_auto_correction(self, reatribution: Reatribution):
        self._query.add_returned_field('authIdHasPrimaryStructure_fs')
        if reatribution.date_end is None:
            date_end = '*'
        else:
            date_end = reatribution.date_end.strftime('%Y')
        if reatribution.date_start is None:
            date_start = '*'
        else:
            date_start = reatribution.date_start.strftime('%Y')
        self._query.clear_filter()
        self._query.change_q('structId_i', str(reatribution.start_element))
        self._query.add_filter('publicationDateY_i', '[' + date_start + ' TO ' + date_end + ']')
        for doc in self._query.responses:
            pub_year = doc['publicationDateY_i']
            pub_month = doc['publicationDateM_i'] if 'publicationDateM_i' in doc.keys() else 1
            pub_day = doc['publicationDateD_i'] if 'publicationDateD_i' in doc.keys() else 1
            pub_date = datetime.date(pub_year, pub_month, pub_day)
            if (isinstance(date_end, str) or pub_date < date_end) and (isinstance(date_start, str) or pub_date > date_start):
                self.new_element.emit([
                    doc['docid'],
                    reatribution.name,
                    str(pub_day) + '/' + str(pub_month) + '/' + str(pub_year),
                    doc['uri_s'],
                    str(reatribution.start_element),
                    str(reatribution.end_element),
                    doc['halId_s'],
                    CorrectTask.STATUS['no_op'],
                    doc['authIdHasPrimaryStructure_fs'],
                ])

    def find_time_end_pub(self, element):
        if element.hal_id not in self._element_done_hal_id:
            self._element_done_hal_id.append(element.hal_id)
            if element.date_end is not None:
                minimal_date = element.date_end if element.date_end > self._start_date else self._start_date
                self._query.clear_filter()
                self._query.change_q('structId_i', str(element.hal_id))
                self._query.add_filter('publicationDateY_i', '[' + (minimal_date + datetime.timedelta(days=1)).strftime('%Y') + ' TO *]')
                for doc in self._query.responses:
                    pub_year = doc['publicationDateY_i']
                    pub_month = doc['publicationDateM_i'] if 'publicationDateM_i' in doc.keys() else 1
                    pub_day = doc['publicationDateD_i'] if 'publicationDateD_i' in doc.keys() else 1
                    pub_date = datetime.date(pub_year, pub_month, pub_day)
                    if pub_date > element.date_end:
                        self.new_element.emit([
                            doc['docid'],
                            str(pub_day) + '/' + str(pub_month) + '/' + str(pub_year),
                            doc['uri_s'],
                            element.hal_id,
                            element.date_end.strftime('%d/%m/%Y'),
                        ])

    def find_ambiguity(self, hal_id, date_end, date_start, ambiguity):
        if date_end is None:
            date_end = '*'
        else:
            date_end = date_end.strftime('%Y')
        if date_start is None:
            date_start = self._start_date.strftime('%Y')
        else:
            date_start = date_start.strftime('%Y') if date_start > self._start_date else self._start_date.strftime('%Y')
        if date_end != '*':
            if int(date_end) < int(date_start):
                return
        self._query.clear_filter()
        self._query.change_q('structId_i', str(hal_id))
        self._query.add_filter('publicationDateY_i', '[' + date_start + ' TO ' + date_end + ']')
        for doc in self._query.responses:
            pub_year = doc['publicationDateY_i']
            pub_month = doc['publicationDateM_i'] if 'publicationDateM_i' in doc.keys() else 1
            pub_day = doc['publicationDateD_i'] if 'publicationDateD_i' in doc.keys() else 1
            pub_date = datetime.date(pub_year, pub_month, pub_day)
            if (isinstance(date_end, str) or pub_date < date_end) and (isinstance(date_start, str) or pub_date > date_start):
                self.new_element.emit([
                    doc['docid'],
                    str(pub_day) + '/' + str(pub_month) + '/' + str(pub_year),
                    doc['uri_s'],
                    hal_id,
                    ambiguity,
                ])

    def set_end_elements(self, end_elements):
        self._end_elements = end_elements

    def set_reatributions(self, reatributions):
        self._reatributions = reatributions

    def run(self):
        if self._finding_type == 'time_end':
            self._element_done_hal_id = []
            totale = len(self._end_elements)
            for nb_done, strucutre_element in enumerate(self._end_elements):
                if self._cancel:
                    break
                self.find_time_end_pub(strucutre_element)
                self.progress_sig.emit(int((nb_done/totale) * 100))
        elif self._finding_type == 'ambiguity':
            totale = len(self._reatributions)
            for nb_done, reatributions in enumerate(self._reatributions):
                if self._cancel:
                    break
                hal_id = reatributions[0].start_element
                ambiguity = ','.join([str(reatribution.end_element) for reatribution in reatributions])
                date_end, date_start = None, None
                for reatribution in reatributions:
                    if reatribution.date_end is not None and (date_end is None or date_end < reatribution.date_end):
                        date_end = reatribution.date_end
                    if reatribution.date_start is not None and (date_start is None or date_start > reatribution.date_start):
                        date_start = reatribution.date_start
                self.find_ambiguity(hal_id, date_end, date_start, ambiguity)
                self.progress_sig.emit(int((nb_done/totale) * 100))
        elif self._finding_type == 'find_auto_correction':
            totale = len(self._reatributions)
            for nb_done, reatribution in enumerate(self._reatributions):
                if self._cancel:
                    break
                self.find_auto_correction(reatribution)
                self.progress_sig.emit(int((nb_done/totale) * 100))



class Controler(QObject):
    clean_reatrib = pyqtSignal()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._db = db
        self._db_filename = None
        db_change_notifier.add_all_events(self)

    def backup_db(self, on_per_day=False):
        if self.db_open:
            file_name = self._db_filename
            if self._db_filename[-3:] == '.db':
                file_name = self._db_filename[:-3]
            new_filename = file_name + '.' + datetime.datetime.today().strftime('%y-%m-%d') + '.db'
            if os.path.isfile(new_filename):
                if on_per_day:
                    return
                new_filename = file_name + '.' + datetime.datetime.today().strftime('%y-%m-%dT%H-%M-%S') + '.db'
            shutil.copyfile(self._db_filename, new_filename)

    @property
    def db(self):
        return self._db

    @property
    def db_open(self) -> bool:
        return self._db_filename is not None

    @property
    def db_filename(self):
        return self._db_filename

    @property
    def db_backups(self):
        if self.db_open:
            files = glob.glob(self._db_filename[:-3] + '.*.db')
            backups = [BachupDB(file) for file in files]
            backups = list(filter(lambda backup: backup.is_valid, backups))
            backups.sort(key=lambda backup: backup.datetime.timestamp())
            return backups
        else:
            return []

    def db_before_saving_element(self, element: Element):
        if element.id is not None:
            old_element = Element.get_by_id(element.id)
            if element.modified_since(old_element):
                element.recalculate_need = True
            if old_element.date_start != element.date_start:
                ne_date_change = ElementDateChange(
                    original_date=old_element.date_start,
                    final_date=element.date_start,
                    element=old_element,
                    is_start=True
                )
                ne_date_change.save()
            if old_element.date_end != element.date_end:
                ne_date_change = ElementDateChange(
                    original_date=old_element.date_end,
                    final_date=element.date_end,
                    element=old_element,
                    is_start=False
                )
                ne_date_change.save()

    def delete_instance(self, instance):
        id_instance = instance.id
        classname = instance.__class__.__name__
        instance.delete_instance()
        db_change_notifier.emit_delete(classname, id_instance)

    def load_database(self, db_filename):
        self._db_filename = db_filename
        self._db.init(db_filename)
        recalculate = False if ParentStructureHal.table_exists() and not (len(ParentStructureHal.select()) <= 0) else True
        self._db.create_tables(model_list)
        if recalculate:
            ParentStructureHal.recalculate_all()
        StructureHal.recalculate_roots()

    def generate_workbook_from_db(self, workbook_filename):
        sheet_list = list(Reatribution.getPeriods().keys())
        wb = Workbook()
        sheets = {}
        default_sheet = sheet_list.pop(0)
        wb.active.title = default_sheet
        sheets[default_sheet] = [wb.active, 2]
        sheets[default_sheet][0]['B1'] = 'Line départ'
        sheets[default_sheet][0]['C1'] = 'Line arrivée'
        for sheet_name in sheet_list:
            sheets[sheet_name] = [wb.create_sheet(sheet_name), 2]
            sheets[sheet_name][0]['B1'] = 'Line départ'
            sheets[sheet_name][0]['C1'] = 'Line arrivée'
        for reatrib in Reatribution.select():
            if reatrib.ambiguities.count() > 0:
                continue
            line = sheets[reatrib.period][1]
            ws = sheets[reatrib.period][0]
            ws['A'+str(line)] = reatrib.name
            ws['B'+str(line)] = reatrib.start_element
            ws['C'+str(line)] = reatrib.end_element
            sheets[reatrib.period][1] += 1
        wb.save(workbook_filename)

    def add_struct(self, docid):
        query_url = "https://api.archives-ouvertes.fr/ref/structure/"
        params = {
            'q': 'docid:' + str(docid),
            'fl': 'docid,acronym_s,label_s,valid_s,type_s,address_s,url_s,endDate_s,startDate_s,parentDocid_i',
        }
        now = datetime.datetime.now()
        response = requests.get(query_url, params)
        last_update = now + response.elapsed
        json_resp = response.json()
        if 'response' in json_resp and 'docs' in json_resp['response']:
            for doc in json_resp['response']['docs']:
                self.save_structure_hal_doc(doc, last_update)

    def save_structure_hal_doc(self, line_doc, last_update):
        docid = line_doc['docid']
        structure_hal = StructureHal.get_or_none(StructureHal.docid==docid)
        if structure_hal is None:
            structure_hal = StructureHal(
                docid = docid,
                last_update = last_update,
                acronym_s = line_doc['acronym_s'] if 'acronym_s' in line_doc.keys() else None,
                label_s = line_doc['label_s'] if 'label_s' in line_doc.keys() else None,
                valid_s = line_doc['valid_s'] if 'valid_s' in line_doc.keys() else None,
                type_s = line_doc['type_s'] if 'type_s' in line_doc.keys() else None,
                address_s = line_doc['address_s'] if 'address_s' in line_doc.keys() else None,
                url_s = line_doc['url_s'] if 'url_s' in line_doc.keys() else None,
                endDate_s = line_doc['endDate_s'][0] if 'endDate_s' in line_doc.keys() else None,
                startDate_s = line_doc['startDate_s'][0] if 'startDate_s' in line_doc.keys() else None,
                parentDocid_i = ','.join(line_doc['parentDocid_i']) if 'parentDocid_i' in line_doc.keys() else None,
            )
        else:
            structure_hal.last_update = last_update
            structure_hal.valid_s = line_doc['valid_s'] if 'valid_s' in line_doc.keys() else None
            structure_hal.url_s = line_doc['url_s'] if 'url_s' in line_doc.keys() else None
            structure_hal.endDate_s = line_doc['endDate_s'][0] if 'endDate_s' in line_doc.keys() else None
            structure_hal.startDate_s = line_doc['startDate_s'][0] if 'startDate_s' in line_doc.keys() else None
            structure_hal.parentDocid_i = ','.join(line_doc['parentDocid_i']) if 'parentDocid_i' in line_doc.keys() else None
        structure_hal.save()

    def generate_db_from_historic_workbook(self, workbook_filename: str, db_filename: str):
        self._db_filename = db_filename
        self._db.init(db_filename)
        self._db.create_tables(model_list)
        wb = load_workbook(workbook_filename)
        labos_sheet = wb['Labos']
        wb_line = 2
        sigle = labos_sheet['A' + str(wb_line)].value
        element_ids = 'BDFH'
        date_ids = 'CEGI'
        while sigle is not None:
            line = Line(label=sigle, acronym=sigle, type='laboratory')
            line.save()
            current = 0
            while len(element_ids) > current:
                id_element = labos_sheet[element_ids[current] + str(wb_line)].value
                if id_element is not None:
                    str_times = str(labos_sheet[date_ids[current] + str(wb_line)].value).split('-')
                    unknowns = [time == '?' for time in str_times]
                    times = [str_date_to_date(str_times[0]), str_date_to_date(str_times[1], True)]
                    element = Element.get_or_none(Element.hal_id == int(id_element))
                    if element is None:
                        element = Element(
                            hal_id = int(id_element),
                            date_start = times[0],
                            unknow_start = unknowns[0],
                            date_end = times[1],
                            unknow_end = unknowns[1],
                        )
                        element.save()
                    line.elements.add(element)
                    line.save()
                current += 1
            wb_line += 1
            sigle = labos_sheet['A' + str(wb_line)].value
        researchteam_sheet = wb['Equipes']
        wb_line = 2
        labo = researchteam_sheet['B' + str(wb_line)].value
        sigle = researchteam_sheet['C' + str(wb_line)].value
        name = labo + ' -> ' + sigle
        element_ids = 'DFHJLNP'
        date_ids = 'EGIKMOQ'
        while sigle is not None:
            line = Line(label=name, acronym=sigle, type='researchteam')
            line.save()
            current = 0
            while len(element_ids) > current:
                id_element = researchteam_sheet[element_ids[current] + str(wb_line)].value
                if id_element is not None:
                    str_times = str(researchteam_sheet[date_ids[current] + str(wb_line)].value).split('-')
                    unknowns = [time == '?' for time in str_times]
                    times = [str_date_to_date(str_times[0]), str_date_to_date(str_times[1], True)]
                    element = Element.get_or_none(Element.hal_id == int(id_element))
                    if element is None:
                        element = Element(
                            hal_id = int(id_element),
                            date_start = times[0],
                            unknow_start = unknowns[0],
                            date_end = times[1],
                            unknow_end = unknowns[1],
                        )
                        element.save()
                    line.elements.add(element)
                    line.save()
                current += 1
            wb_line += 1
            labo = researchteam_sheet['B' + str(wb_line)].value
            sigle = researchteam_sheet['C' + str(wb_line)].value
            if labo is not None:
                name = labo + ' -> ' + sigle
            else:
                name = sigle
        department_sheet = wb['Départements']
        wb_line = 2
        sigle = department_sheet['A' + str(wb_line)].value
        element_ids = 'BDFH'
        date_ids = 'CEGI'
        while sigle is not None:
            line = Line(label=sigle, acronym=sigle, type='department')
            line.save()
            current = 0
            while len(element_ids) > current:
                id_element = department_sheet[element_ids[current] + str(wb_line)].value
                if id_element is not None:
                    str_times = str(department_sheet[date_ids[current] + str(wb_line)].value).split('-')
                    unknowns = [time == '?' for time in str_times]
                    times = [str_date_to_date(str_times[0]), str_date_to_date(str_times[1], True)]
                    element = Element.get_or_none(Element.hal_id == int(id_element))
                    if element is None:
                        element = Element(
                            hal_id = int(id_element),
                            date_start = times[0],
                            unknow_start = unknowns[0],
                            date_end = times[1],
                            unknow_end = unknowns[1],
                        )
                        element.save()
                    line.elements.add(element)
                    line.save()
                current += 1
            wb_line += 1
            sigle = department_sheet['A' + str(wb_line)].value
        regrouplaboratory_sheet = wb['Regroupement de labos']
        wb_line = 2
        sigle = regrouplaboratory_sheet['A' + str(wb_line)].value
        element_ids = 'BDFH'
        date_ids = 'CEGI'
        while sigle is not None:
            line = Line(label=sigle, acronym=sigle, type='regrouplaboratory')
            line.save()
            current = 0
            while len(element_ids) > current:
                id_element = regrouplaboratory_sheet[element_ids[current] + str(wb_line)].value
                if id_element is not None:
                    str_times = str(regrouplaboratory_sheet[date_ids[current] + str(wb_line)].value).split('-')
                    unknowns = [time == '?' for time in str_times]
                    times = [str_date_to_date(str_times[0]), str_date_to_date(str_times[1], True)]
                    element = Element.get_or_none(Element.hal_id == int(id_element))
                    if element is None:
                        element = Element(
                            hal_id = int(id_element),
                            date_start = times[0],
                            unknow_start = unknowns[0],
                            date_end = times[1],
                            unknow_end = unknowns[1],
                        )
                        element.save()
                    line.elements.add(element)
                    line.save()
                current += 1
            wb_line += 1
            sigle = regrouplaboratory_sheet['A' + str(wb_line)].value

    def recalculate_all_reatrib(self):
        Element.update(recalculate_need=True).execute()
        for reatrib in Reatribution.select():
            reatrib.ambiguities.clear()
        Reatribution.delete().execute()
        self.clean_reatrib.emit()
        self.recalculate_elements()

    def recalculate_elements(self):
        elements_done = []
        for element in Element.select().where(Element.recalculate_need == True):
            if element.hal_id not in elements_done:
                elements_done.append(element.hal_id)
                for line in element.lines:
                    elements_same_line = [element for element in line.elements.order_by(Element.date_start, -Element.date_end)]
                    for el_index, element_same_line in enumerate(elements_same_line):
                        if element_same_line != element:
                            # Ajout élément => élément same line
                            date_start = element_same_line.date_start
                            if el_index <= 0:
                                date_start = None
                            date_end = element_same_line.date_end
                            if date_end is None and date_start is None:
                                continue
                            reatribution = Reatribution.get_or_none(
                                (Reatribution.start_element == element.hal_id) &
                                (Reatribution.end_element == element_same_line.hal_id)
                            )
                            if reatribution is None:
                                reatribution = Reatribution(
                                    name = line.acronym,
                                    date_start = date_start,
                                    date_end = date_end,
                                    start_element = element.hal_id,
                                    end_element = element_same_line.hal_id,
                                )
                            else:
                                reatribution.date_end = date_end
                                reatribution.date_start = date_start
                            reatribution.save()
                element.recalculate_need = False
                element.save()
        start_elements = []
        # check ambiguity
        for ambiguity in Ambiguity.select():
            ambiguity.reatributions.clear()
        Ambiguity.delete().execute()
        for reatrib in Reatribution.select():
            if reatrib.start_element not in start_elements:
                start_elements.append(reatrib.start_element)
                reatribs_same_start = [reatrib for reatrib in Reatribution.select().where(
                        (Reatribution.start_element == reatrib.start_element)
                    )]
                for reatrib1, reatrib2 in itertools.combinations(reatribs_same_start, 2):
                    if reatrib1.overlap_date(reatrib2):
                        start_element = Element.get(Element.hal_id == reatrib.start_element)
                        ambiguity = Ambiguity.get_or_none(start_element=start_element, date_start=reatrib.date_start, date_end=reatrib.date_end)
                        if ambiguity is None:
                            ambiguity = Ambiguity(start_element=start_element, date_start=reatrib.date_start, date_end=reatrib.date_end)
                            ambiguity.save()
                        if reatrib1 not in ambiguity.reatributions:
                            ambiguity.reatributions.add(reatrib1)
                        if reatrib2 not in ambiguity.reatributions:
                            ambiguity.reatributions.add(reatrib2)
