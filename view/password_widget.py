from PyQt6.QtWidgets import QWidget
from PyQt6 import uic
from PyQt6.QtCore import pyqtSignal
from view.option_widget import Options

import pathlib
ui_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'ui')
password_widget_file = pathlib.Path.joinpath(ui_dir, 'password_widget.ui')

import keyring
from keyring.backends import Windows
keyring.set_keyring(Windows.WinVaultKeyring())


class PasswordWidget(QWidget):
    validated = pyqtSignal(str, str)

    def __init__(self, options: Options, *args, **kwargs):
        super().__init__(*args, **kwargs)
        uic.loadUi(password_widget_file, self)

        self._options = options
        self.login_lineedit.setText(self._options.login)
        password = keyring.get_password("halto", self._options.login)
        if password is not None:
            self.password_lineedit.setText(password)
            self.save_password_checkbox.setChecked(True)

        self.validate_button.pressed.connect(self.validate)

    def validate(self):
        login = self.login_lineedit.text()
        self._options.login = login
        self._options.save()
        password = self.password_lineedit.text()
        if self.save_password_checkbox.isChecked():
            keyring.set_password("halto", login, password)
        else:
            if keyring.get_password("halto", login) is not None:
                keyring.delete_password("halto", login)
        self.validated.emit(login, password)
        self.close()

    def password_saved(self):
        return self.save_password_checkbox.isChecked()
