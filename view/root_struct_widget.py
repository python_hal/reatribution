from PyQt6.QtWidgets import QWidget, QTableWidgetItem, QPushButton, QFileDialog, QMessageBox
from PyQt6 import uic
from PyQt6.QtCore import Qt
from model.database import *
from controler.controler import Controler
from view.element_widget import ElementWidget
from view.line_widget import LineWidget
from view.struct_widget import StructWidget
import requests
import webbrowser
import json

import pathlib
ui_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'ui')
root_struct_widget_file = pathlib.Path.joinpath(ui_dir, 'root_struct_widget.ui')


class RootStructWidget(QWidget):
    def __init__(self, controler:Controler, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        uic.loadUi(root_struct_widget_file, self)
        self._controler = controler
        self._root_list = []
        self._ids2rows = {}
        self._info_struct = {}

        self.find_parent_button.clicked.connect(self.find_parent)
        self.struct_table.itemSelectionChanged.connect(self.select_struct)
        self.parent_table.itemSelectionChanged.connect(self.select_parent_struct)
        self.open_aurehal_button.clicked.connect(self.open_aurehal)
        self.export_root_button.clicked.connect(self.export_root)
        db_change_notifier.add_all_events(self)

    def db_new_structure_hal(self, _: StructureHal):
        self.find_parent()

    def db_delete_structure_hal(self, _):
        """
        db_delete_event: StructureHal;
        """
        self.find_parent()

    def export_root(self):
        save_file_name, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrer liste sutructure racines',
            str(pathlib.Path.home()),
            'liste json (*.json)'
        )
        if save_file_name != '':
            with open(save_file_name, "w") as save_file:
                json.dump(self._root_list, save_file)

    def open_aurehal(self):
        if self.struct_table.currentItem() is not None:
            row = self.struct_table.currentItem().row()
            docid = self.struct_table.item(row, 0).text()
            webbrowser.open('https://aurehal.archives-ouvertes.fr/structure/read/id/' + docid)
        else:
            QMessageBox.warning(self, "Pas de structure séléctionner", "Pas de structure séléctionner.")

    def select_parent_struct(self):
        if self.parent_table.currentItem() is not None:
            layout = self.parent_struct_widget.layout()
            for i in reversed(range(layout.count())):
                layout.itemAt(i).widget().setParent(None)
            self.get_info_struct(self.parent_table.currentItem().data(0x0100))

    def select_struct(self):
        self.parent_table.setRowCount(0)
        if self.struct_table.currentItem() is not None:
            row = self.struct_table.currentItem().row()
            id_item = self.struct_table.item(row, 0).data(0x0100)
            structure_hal = StructureHal.get_or_none(StructureHal.id == id_item)
            layout = self.struct_widget.layout()
            for i in reversed(range(layout.count())):
                layout.itemAt(i).widget().setParent(None)
            layout.addWidget(StructWidget(structure_hal))
            layout = self.element_widget.layout()
            for i in reversed(range(layout.count())):
                layout.itemAt(i).widget().setParent(None)
            element_selected = Element.get_or_none(Element.hal_id == structure_hal.docid)
            if element_selected is not None:
                self.struct_tab.setTabEnabled(1, True)
                new_element_widget = ElementWidget(element_selected, structure_hal=structure_hal)
                layout.addWidget(new_element_widget)
                for _ in range(self.line_toolbox.count()):
                    self.line_toolbox.removeItem(0)
                for line in element_selected.lines:
                    self.line_toolbox.addItem(LineWidget(line), line.acronym)
            else:
                if self.struct_tab.currentIndex() == 1:
                    self.struct_tab.setCurrentIndex(0)
                self.struct_tab.setTabEnabled(1, False)
            self.child_struct_table.setRowCount(0)
            if structure_hal.parentDocid_i is not None:
                for parent in structure_hal.parentDocid_i.split(','):
                    add_button = QPushButton('Ajouter')
                    add_button.clicked.connect(self.make_slot(self._controler.add_struct, parent))
                    parent_id = int(parent)
                    if parent_id not in self._info_struct:
                        acronym = ''
                        label = ''
                    else:
                        struct = self._info_struct[parent_id]
                        acronym = struct['acronym_s'] if 'acronym_s' in struct else ''
                        label = struct['label_s'] if 'label_s' in struct else ''
                    struct_data = [
                        str(parent),
                        acronym,
                        label,
                        add_button,
                    ]
                    self.add_row_to_table(struct_data, self.parent_table, int(parent))
            for parent_rel in ParentStructureHal.select().where(ParentStructureHal.parent == structure_hal):
                struct = parent_rel.child
                info_button = QPushButton('Afficher info')
                info_button.clicked.connect(self.make_slot(self.show_struct, struct))
                struct_data = [
                    str(struct.docid),
                    struct.acronym_s,
                    struct.label_s,
                    struct.parentDocid_i,
                    info_button
                ]
                self.add_row_to_table(struct_data, self.child_struct_table, struct.id)

    def make_slot(self, callback:callable, docid:int):
        def slot():
            callback(docid)
        return slot

    def show_struct(self, struct):
        self._struct_widget = StructWidget(struct)
        self._struct_widget.show()

    def show_parent_struct(self, struct):
        layout = self.parent_struct_widget.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)
        layout.addWidget(StructWidget(struct))
        struct_data = [
            str(struct['docid']),
            struct['acronym_s'] if 'acronym_s' in struct else '',
            struct['label_s'] if 'label_s' in struct else '',
        ]
        self.update_row_table(struct_data, self.parent_table, row_id  = int(struct['docid']))

    def get_info_struct(self, docid):
        if docid not in self._info_struct:
            query_url = "https://api.archives-ouvertes.fr/ref/structure/"
            params = {
                'q': 'docid:' + str(docid),
                'fl': 'docid,acronym_s,label_s,valid_s,type_s,address_s,url_s,endDate_s,startDate_s,parentDocid_i',
            }
            response = requests.get(query_url, params)
            try:
                json_resp = response.json()
            except requests.exceptions.JSONDecodeError:
                QMessageBox.warning(self, "Erreur", "erreur de téléchargement des données.")
                return
            if 'response' in json_resp and 'docs' in json_resp['response'] and len(json_resp['response']['docs']) >= 1:
                self._info_struct[docid] = json_resp['response']['docs'][0]
        self.show_parent_struct(self._info_struct[docid])

    def add_row_to_table(self, row_data, table, row_id=None):
        row = table.rowCount()
        table.setRowCount(row+1)
        for col, item in enumerate(row_data):
            if item is None:
                item = ''
            if isinstance(item, QWidget):
                cell = item
                table.setCellWidget(row, col, cell)
            else:
                cell = QTableWidgetItem(str(item))
                cell.setFlags(cell.flags() ^ Qt.ItemFlag.ItemIsEditable)
                table.setItem(row, col, cell)
                if row_id is not None:
                    cell.setData(0x0100, row_id)
            if table.objectName() not in self._ids2rows:
                self._ids2rows[table.objectName()] = {}
            self._ids2rows[table.objectName()][row_id] = row

    def update_row_table(self, row_data, table, row=None, row_id=None):
        if row is None:
            row = self._ids2rows[table.objectName()][row_id]
        for col, item in enumerate(row_data):
            if item is None:
                item = ''
            if table.item(row, col) is not None:
                table.item(row, col).setText(str(item))
            else:
                table.cellWidget(row, col).setText(str(item))

    def delete_row_table(self, table, row=None, row_id=None):
        """
        supprime une ligne dans un tableau
        """
        if row is None:
            row = self._ids2rows[table.objectName()][row_id]
            del self._ids2rows[table.objectName()][row_id]
            for current_id in self._ids2rows[table.objectName()].keys():
                if self._ids2rows[table.objectName()][current_id] > row:
                    self._ids2rows[table.objectName()][current_id] -= 1
        table.removeRow(row)

    def build_del_struct(self, root_struct: StructureHal):
        def del_struct(struct: StructureHal):
            element = Element.get_or_none(Element.hal_id == struct.docid)
            if element is not None:
                for line in element.lines:
                    line.elements.remove(line)
                self._controler.delete_instance(element)
            self._controler.delete_instance(struct)
        return lambda: del_struct(root_struct)

    def find_parent(self):
        self.struct_table.setRowCount(0)
        self._root_list = []
        structs_check = {}
        for struct in StructureHal.select():
            is_root = True
            if struct.parentDocid_i is not None:
                for parent in struct.parentDocid_i.split(','):
                    if parent not in structs_check.keys():
                        structs_check[parent] = StructureHal.select().where(StructureHal.docid == parent).exists()
                    if structs_check[parent]:
                        is_root = False
                        break
            if is_root:
                del_button = QPushButton('Suppression')
                del_button.clicked.connect(self.build_del_struct(struct))
                struct_data = [
                    str(struct.docid),
                    struct.acronym_s,
                    struct.label_s,
                    struct.parentDocid_i,
                    del_button
                ]
                self._root_list.append(struct.docid)
                self.add_row_to_table(struct_data, self.struct_table, struct.id)
