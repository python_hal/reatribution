# import PyQt6
from PyQt6.QtWidgets import (
    QMainWindow, QFileDialog, QTableWidgetItem, QMessageBox,
    QInputDialog, QWidget, QTableWidget, QApplication,
    QMenu, QComboBox, QLineEdit, QHeaderView, QLabel
)
from PyQt6 import uic
from PyQt6 import QtGui
from PyQt6.QtCore import Qt

# import HalTo
from model.database import *
from controler.controler import Controler, UpdateThread, ErrorFindingThread, CorrectDocThread
from view.element_widget import ElementWidget
from view.line_widget import LineWidget
from view.graphviz_gen.graph_gen import StructbWithReatrib
from view.graphviz_gen.graph_gen import ReatribtionGraph
from view.inconsistencies_check_widget import InconsistenciesCheckWidget
from view.root_struct_widget import RootStructWidget
from view.password_widget import PasswordWidget
from view.option_widget import OptionWidget
from model.option import Options, AutoBackupOption

# import other package
from openpyxl import Workbook

# import Python
from exception_handling import ExceptionHandler
import os
import inspect
import shutil
import string
import webbrowser
import json
import re

# access paths
import pathlib
ui_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'ui')
main_windows_file = pathlib.Path.joinpath(ui_dir, 'main_window.ui')
same_search_file = pathlib.Path.joinpath(ui_dir, 'same_search.ui')
app_dir = pathlib.Path(__file__).parent.parent.resolve()


def check_db(met):
    def new_met(self, *args, **kwargs):
        if not self._controler.db_open:
            QMessageBox.warning(self, "Pas de base de donnée ouverte", "Vous devez ouvrir une base de données pour ça.")
            return
        args = list(args)
        sig = inspect.signature(met)
        nb_pos_args = [param.default == param.empty for param in sig.parameters.values()].count(True)
        while len(args) + 1 > nb_pos_args:
            args.pop()
        return met(self, *args, **kwargs)
    return new_met


class SameSearchTableWidget(QWidget):
    search_icon = None
    cancel_icon = None
    def __init__(self, text: str, search: callable, unsearch: callable, main_window, *args, **kwargs):
        super().__init__(*args, **kwargs)
        uic.loadUi(same_search_file, self)
        self.label.setText(text)
        self._data = None
        self._search = search
        self._unsearch = unsearch
        self._is_unserch = True
        self._text = text
        self._main_window = main_window
        if self.search_icon is None:
            self.search_icon = QtGui.QIcon(str(app_dir / 'img/search.ico'))
        if self.cancel_icon is None:
            self.cancel_icon = QtGui.QIcon(str(app_dir / 'img/cancel.ico'))
        self.tool_button.setIcon(self.search_icon)
        self.tool_button.clicked.connect(self.search)

    def unsearch(self):
        self._is_unserch = True
        self.tool_button.setIcon(self.search_icon)

    def search(self):
        if self._is_unserch:
            if self._search is not None:
                self._search(self._text)
            self.tool_button.setIcon(self.cancel_icon)
            self._is_unserch = False
            self._main_window.activate_same_search(self)
        else:
            if self._unsearch is not None:
                self._unsearch()
            self.tool_button.setIcon(self.search_icon)
            self._is_unserch = True

    def text(self):
        return self._text

    def setText(self, text: str):
        self._text = text
        self.label.setText(text)

    def setData(self, _, data):
        self._data = data

    def data(self, _):
        return self._data


def export_table_to_xlsx(table, filename, headers=None, col_edits=None, select_row=None, hide_col=[]):
    def no_edit(col): return col
    if select_row is None:
        select_row = lambda row: not table.isRowHidden(row)
    if col_edits is None: col_edits = {}
    save_wb = Workbook()
    sheet = save_wb.active
    line = 1
    if headers is not None:
        line += 1
        alphabet = string.ascii_uppercase
        for col_id, header in enumerate(headers):
            sheet[alphabet[col_id] + '1'] = header
    for row in range(table.rowCount()):
        if select_row(row):
            xlsx_col_id = -1
            for col_id in range(table.columnCount()):
                if col_id in hide_col:
                    continue
                else:
                    xlsx_col_id += 1
                col_edit = no_edit if col_id not in col_edits else col_edits[col_id]
                col = alphabet[xlsx_col_id]
                item = table.item(row, col_id)
                if item is None:
                    item = table.cellWidget(row, col_id)
                sheet[col + str(line)] = col_edit(item.text())
            line += 1
    save_wb.save(filename=filename)


def make_filer_line(table, combo, line, indexes=None):
    if indexes is None:
        indexes = range(combo.count()-1)
    if combo.currentIndex() != 0:
        cols = [indexes[combo.currentIndex()-1]]
    else:
        cols = indexes
    search_text = line.text()
    if isinstance(table, QTableWidget):
        get_item = lambda row:[table.item(row, col).text() if table.item(row, col) is not None
                               else table.cellWidget(row, col).text() if table.cellWidget(row, col) is not None else None
                               for col in cols]
    else:
        get_item = lambda row:[table.model().index(row, col).data() for col in cols if table.model().index(row, col) is not None]
    def new_filter(row):
        item_text = get_item(row)
        for item_value in item_text:
            if item_value is not None and search_text.casefold() in item_value.casefold():
                return False
        return True
    return new_filter


class MainWindow(QMainWindow):
    OPEN_DB_MESSAGE = "Base de donnée courante : "

    """ Fenétre principale
    """
    def __init__(self, exception_handler: ExceptionHandler, *args, **kwargs):
        super().__init__(*args, **kwargs)
        uic.loadUi(main_windows_file, self)
        self.setWindowIcon(QtGui.QIcon(str(app_dir / 'img/halto.ico')))
        exception_handler.add_handler(self.show_warning)
        self._controler = Controler()
        self._controler.clean_reatrib.connect(self.clean_reatrib_table)
        self._doc_error = []
        self._doc_time_end = []
        self._periods = []
        self._query_hal_thread = None
        self._error_finding_thread = None
        self._ids2rows = {}
        self._same_search_table_activated = None
        self._cancel_icon = QtGui.QIcon(str(app_dir / 'img/cancel.ico'))
        self._parent_structs_check = {}
        self._options = Options()
        self._ignore_struct = {}
        self._correct_doc_thread = None
        self._no_element_hal_id = []
        self._no_struct_id = []
        self._skip_search_new = False
    
        self._status_bar = self.statusBar()
        self._db_open_message = QLabel(self.OPEN_DB_MESSAGE)
        self._status_bar.addPermanentWidget(self._db_open_message)

        # Mascage label warning
        self.warning_label.setVisible(False)
        self.warning_ico_label.setVisible(False)
        self.warning_label_2.setVisible(False)
        self.warning_ico_label_2.setVisible(False)

        # assignation model à view
        self.ignored_struct_table_view.setModel(self.structure_hal_table.model())
        self.struct_line_table_view.setModel(self.line_table.model())

        # Cache colonne
        self.structure_hal_table.setColumnHidden(12, True)
        self.structure_hal_table.setColumnHidden(3, True)
        self.ignored_struct_table_view.setColumnHidden(1, True)
        self.ignored_struct_table_view.setColumnHidden(2, True)
        self.ignored_struct_table_view.setColumnHidden(7, True)
        self.ignored_struct_table_view.setColumnHidden(8, True)
        self.ignored_struct_table_view.setColumnHidden(9, True)
        self.ignored_struct_table_view.setColumnHidden(10, True)
        self.ignored_struct_table_view.setColumnHidden(11, True)

        # Ajout icon
        self.stop_error_op_button.setIcon(self._cancel_icon)
        self.stop_hal_op_button.setIcon(self._cancel_icon)
        self.stop_auto_correction_op_button.setIcon(self._cancel_icon)
        self.stop_time_end_op_button.setIcon(self._cancel_icon)
        self.cancel_auto_correcttion_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_line_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_element_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_hal_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_reatrib_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_error_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_hal_struct_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_time_end_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_structure_task_search_toolbutton.setIcon(self._cancel_icon)
        self.cancel_ignored_struct_search_toolbutton.setIcon(self._cancel_icon)

        # ajout événement interface graphique

        ## événement menu
        ### menu fichier
        self.open_db_action.triggered.connect(self.open_database)
        self.reopen_db_action.triggered.connect(self.reopen_database)
        self.create_database_action.triggered.connect(self.create_database)
        self.option_action.triggered.connect(self.show_option)
        self.quit_action.triggered.connect(QApplication.instance().exit)
        self.backup_db_action.triggered.connect(self.backup_db)
        ### menu exportation
        self.create_xlsx_file_action.triggered.connect(self.save_xlsx)
        self.export_log_action.triggered.connect(self.export_log)
        ### menu vérification
        self.check_elements_date_action.triggered.connect(self.check_elements_date)
        self.check_root_struct_action.triggered.connect(self.check_root_struct)
        ### menu aide
        self.help_index_action.triggered.connect(self.help_index)

        ## onglet
        ### Base de données
        #### Line
        self.line_table.itemSelectionChanged.connect(self.new_line_select)
        self.line_table.customContextMenuRequested.connect(self.line_table_right_click)
        self.gen_struct_graph_button.pressed.connect(self.gen_struct_graph)
        self.element_without_struct_checkbox.clicked.connect(self.element_without_struct)
        self.line_export_xlsx_button.clicked.connect(self.line_export_xlsx)
        #### Element
        self.element_table.itemSelectionChanged.connect(self.new_element_select)
        self.element_table.customContextMenuRequested.connect(self.element_table_right_click)
        self.open_element_aurehal_button.clicked.connect(self.open_aurehal(self.element_table))
        self.element_export_xlsx_button.clicked.connect(self.element_export_xlsx)
        self.without_stuct_check.stateChanged.connect(self.element_search)
        ### Réatribution
        self.recalculate_all_reatrib_button.clicked.connect(self.recalculate_all_reatrib)
        self.recalculate_all_reatrib_button_2.clicked.connect(self.recalculate_all_reatrib)
        self.gen_all_reatrib_button.pressed.connect(self.gen_all_reatrib)
        self.gen_reatrib_button.pressed.connect(self.gen_reatrib_graph)
        self.export_reatrib_xlsx_button.clicked.connect(self.export_reatrib_xlsx)
        self.reatribute_period_combo.currentTextChanged.connect(self.select_period)
        self.show_all_reatrib_radio.clicked.connect(self.show_reatrib_amb)
        self.show_no_amb_reatrib_radio.clicked.connect(self.show_reatrib_amb)
        self.show_amb_reatrib_radio.clicked.connect(self.show_reatrib_amb)
        ### Publication en erreur
        #### autocorrection
        self.ignore_pub_button.clicked.connect(self.ignore_pub)
        self.auto_correcttion_table.itemSelectionChanged.connect(self.auto_correcttion_select)
        self.stop_auto_correction_op_button.clicked.connect(self.stop_auto_correction_op)
        self.find_auto_correction_button.pressed.connect(self.find_auto_correction)
        self.export_auto_correcttion_xlsx_button.clicked.connect(self.export_auto_correcttion_xlsx)
        self.correct_selected_pub_button.clicked.connect(self.correct_selected_pub)
        self.correct_all_pub_button.clicked.connect(self.correct_all_pub)
        self.del_backup_button.clicked.connect(self.del_backup)
        #### ambiquity
        self.stop_error_op_button.clicked.connect(self.stop_error_op)
        self.find_ambiguity_pub_button.pressed.connect(self.find_ambiguity_pub)
        self.pub_error_table.cellDoubleClicked.connect(self.open_pub_error_url)
        self.export_erreur_xlsx_button.pressed.connect(self.export_erreur_xlsx)
        #### after end
        self.stop_time_end_op_button.clicked.connect(self.stop_time_end_op)
        self.find_time_end_pub_button.pressed.connect(self.find_time_end_pub)
        self.export_time_end_xlsx_button.pressed.connect(self.export_time_end_xlsx)
        self.show_ignored_auto_corr_checkbox.stateChanged.connect(self.error_auto_correcttion)
        ### Import Hal
        #### structure hal
        self.structure_hal_table.customContextMenuRequested.connect(self.structure_hal_right_click)
        self.structure_hal_table.cellClicked.connect(self.new_struct_hal_select)
        self.structure_hal_table.cellDoubleClicked.connect(self.open_struct_url)
        self.export_structure_hal_xlsx_button.clicked.connect(self.export_structure_hal_xlsx)
        self.search_line_button.pressed.connect(self.search_struct)
        self.add_struct_button.pressed.connect(self.add_struct)
        self.import_root_button.clicked.connect(self.import_root)
        #### affichage
        self.show_struct_mod_combo.currentIndexChanged.connect(self.refresh_structure_hal_table)
        self.show_valid_checkbox.stateChanged.connect(self.refresh_structure_hal_table)
        self.show_old_checkbox.stateChanged.connect(self.refresh_structure_hal_table)
        self.only_root_struct_checkbox.stateChanged.connect(self.refresh_structure_hal_table)
        #### line
        self.struct_line_table_view.selectionModel().selectionChanged.connect(self.structure_hal_new_struct_select)
        self.hal_create_struct_button.pressed.connect(self.create_new_line_from_hal)
        #### MàJ
        self.stop_hal_op_button.clicked.connect(self.stop_hal_op)
        self.update_queries_button.pressed.connect(self.update_queries)
        #### Add Task
        self.add_task_button.clicked.connect(self.add_task)
        #### Add ignore
        self.ignore_structure_button.clicked.connect(self.ignore_structure)
        ### Task
        self.structure_task_table.itemSelectionChanged.connect(self.structure_task_select)
        self.task_combo.currentIndexChanged.connect(self.task_select)
        self.save_task_button.clicked.connect(self.save_task)
        self.del_task_button.clicked.connect(self.del_task)
        ### Ignorer
        self.del_ignored_button.clicked.connect(self.del_ignored)
        self.ignored_struct_table_view.clicked.connect(self.structure_ignore_select)
        self.save_ignored_button.clicked.connect(self.save_ignored)
        self.ignore_export_xlsx_button.clicked.connect(self.ignore_export_xlsx)

        self.line_table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Interactive)
        self.element_table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Interactive)
        self.reatribute_table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Interactive)
        self.structure_hal_table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Interactive)
        self.structure_task_table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Interactive)

        # connection signal recherche
        def connect_search_widgets(line, combo, cancel_button, search):
            cancel_button.clicked.connect(self.clean_serch(line, combo, search))
            line.textChanged.connect(search)
            combo.currentTextChanged.connect(search)

        connect_search_widgets(self.line_search_line, self.line_search_combo, self.cancel_line_search_toolbutton, self.line_search)
        connect_search_widgets(self.element_search_line, self.element_search_combo, self.cancel_element_search_toolbutton, self.element_search)
        connect_search_widgets(self.reatrib_search_line, self.reatrib_search_combo, self.cancel_reatrib_search_toolbutton, self.reatrib_search)
        connect_search_widgets(self.error_search_lline, self.error_search_combo, self.cancel_error_search_toolbutton, self.error_search)
        connect_search_widgets(self.hal_struct_search_line, self.hal_struct_search_combo, self.cancel_hal_struct_search_toolbutton, self.hal_struct_hal_search)
        connect_search_widgets(self.hal_search_line, self.hal_search_combo, self.cancel_hal_search_toolbutton, self.refresh_structure_hal_table)
        connect_search_widgets(self.auto_correcttion_search_line, self.auto_correcttion_combo, self.cancel_auto_correcttion_search_toolbutton, self.error_auto_correcttion)
        connect_search_widgets(self.time_end_search_lline, self.time_end_search_combo, self.cancel_time_end_search_toolbutton, self.time_end_search)
        connect_search_widgets(self.structure_task_search_line, self.structure_task_search_combo, self.cancel_structure_task_search_toolbutton, self.structure_task_search)
        connect_search_widgets(self.ignored_struct_search_line, self.ignored_struct_search_combo, self.cancel_ignored_struct_search_toolbutton, self.ignored_struct_search)

        # event database registration
        db_change_notifier.add_all_events(self)

    def export_log(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Ouvrir une base de donnée',
            str(pathlib.Path.home()),
            'log (*.log)'
        )
        if filename!='':
            error_log_file = app_dir / 'error.log'
            shutil.copyfile(error_log_file, filename)

    @check_db
    def backup_db(self):
        self._controler.backup_db()
        QMessageBox.information(self, "Sauvegarde effectuer", "La sauvegarde de la base de donnée à bien était effectuer.")

    def show_warning(self):
        QMessageBox.warning(self, "Une erreur inconnue est survenue", "Vous devriez sauvegarder des que possible (en faisant une sauvegarde de la base de données) et relancer le logiciel.")

    def recalculate_all_reatrib(self):
        self._controler.recalculate_all_reatrib()
        self.warning_label.setVisible(False)
        self.warning_ico_label.setVisible(False)
        self.warning_label_2.setVisible(False)
        self.warning_ico_label_2.setVisible(False)

    def del_task(self):
        selected_task = StructureTask.get_or_none(StructureTask.id == self.task_combo.currentData(0x0100))
        if selected_task:
            self._controler.delete_instance(selected_task)

    def save_task(self):
        selected_task = StructureTask.get_or_none(StructureTask.id == self.task_combo.currentData(0x0100))
        if selected_task:
            selected_task.message = self.task_combo.currentText()
            selected_task.details = self.task_details_text.toPlainText()
            selected_task.done = self.task_done_check.isChecked()
            selected_task.save()

    def save_ignored(self):
        row = self.ignored_struct_table_view.currentIndex().row()
        if row >= 0:
            id_struct = self.structure_hal_table.item(row, 0).data(0x0100)
            struct = StructureHal.get_by_id(id_struct)
            ignore = struct.ignore.get()
            if ignore:
                ignore.message = self.ignored_message_line.text()
                ignore.details = self.ignored_details_text.toPlainText()
                ignore.save()

    @check_db
    def del_ignored(self):
        current_row = self.ignored_struct_table_view.currentIndex().row()
        struct = StructureHal.get_by_id(self.structure_hal_table.item(current_row, 0).data(0x0100))
        struct_igored = StructureIgnore.get_or_none(StructureIgnore.structure == struct)
        if struct_igored is not None:
            self._controler.delete_instance(struct_igored)

    def task_select(self, index):
        selected_task = StructureTask.get_or_none(StructureTask.id == self.task_combo.itemData(index, 0x0100))
        if selected_task:
            self.task_details_text.setPlainText(selected_task.details)
            self.task_done_check.setChecked(selected_task.done)

    def structure_task_select(self):
        self.task_combo.clear()
        self.task_combo.clearEditText()
        row = self.structure_task_table.currentRow()
        if row >= 0:
            self.task_combo.setEnabled(True)
            struct_id = self.structure_task_table.item(row, 0).data(0x0100)
            struct = StructureHal.get_by_id(struct_id)
            for task in StructureTask.select().where(StructureTask.structure==struct):
                self.task_combo.addItem(task.message, task.id)

    def structure_ignore_select(self):
        row = self.ignored_struct_table_view.currentIndex().row()
        if row >= 0:
            id_struct = self.structure_hal_table.item(row, 0).data(0x0100)
            struct = StructureHal.get_by_id(id_struct)
            ignore = struct.ignore.get()
            self.ignored_message_line.setText(ignore.message)
            self.ignored_details_text.setPlainText(ignore.details)

    def add_task(self):
        row = self.structure_hal_table.currentRow()
        if row >= 0:
            struct_id = self.structure_hal_table.item(row, 0).data(0x0100)
            struct = StructureHal.get_by_id(struct_id)
            task = StructureTask(structure=struct, message=self.task_line.text())
            task.save()

    def ignore_structure(self):
        ignore_message = self.ignore_line.text()
        if ignore_message != '':
            row = self.structure_hal_table.currentRow()
            if row >= 0:
                struct = StructureHal.get_by_id(self.structure_hal_table.item(row, 0).data(0x0100))
                ignore = StructureIgnore(structure=struct, message=ignore_message)
                ignore.save()
        else:
            QMessageBox.warning(self, "Message nécéssaire", "Vous devez ajouter un message.")

    def auto_correcttion_select(self):
        self.del_backup_button.setEnabled(False)
        self.restaure_auto_correction_button.setEnabled(False)
        if self.auto_correcttion_table.currentItem() is not None:
            row = self.auto_correcttion_table.currentItem().row()
            correction = CorrectionPub.get_or_none(CorrectionPub.docid == self.auto_correcttion_table.item(row, 0).text())
            if correction and correction.has_backup:
                self.del_backup_button.setEnabled(True)
                self.restaure_auto_correction_button.setEnabled(True)

    def del_backup(self):
        response = QMessageBox.question(self, 'Suppression backup', 'Êtes-vous sure de vouloire supprimer le backup de la publication selectionner ?')
        if response == QMessageBox.StandardButton.Yes.value and self.auto_correcttion_table.currentItem() is not None:
            row = self.auto_correcttion_table.currentItem().row()
            correction = CorrectionPub.get_or_none(CorrectionPub.docid == self.auto_correcttion_table.item(row, 0).text())
            for file in correction.backup_files:
                if os.path.exists(file):
                    os.remove(file)
            self.del_backup_button.setEnabled(False)
            self.restaure_auto_correction_button.setEnabled(False)


    def add_task_to_correct_doc(self, rows):
        for row in rows:
            docid = self.auto_correcttion_table.item(row, 0).text()
            start_forme = self.auto_correcttion_table.item(row, 4).text()
            end_forme = self.auto_correcttion_table.item(row, 5).text()
            halId_s = self.auto_correcttion_table.item(row, 6).text()
            self._correct_doc_thread.add_task(docid, start_forme, end_forme, halId_s, row)

    def correct_all_pub(self):
        if self._options.instance is None:
            QMessageBox.warning(self, "Pas de protail selectionner", "Vous devez allez dans préférence pour choisir un portail.")
            return
        else:
            self._correct_doc_thread = CorrectDocThread(self._options.instance.code)
        self.auto_correction_current_op_label.setText('Opération courante : Corriger les publication selectionner')
        self.add_task_to_correct_doc(range(self.auto_correcttion_table.rowCount()))
        self._correct_doc_thread.progress_sig.connect(self.correct_selected_pub_progress)
        self._correct_doc_thread.finished.connect(self.correct_selected_pub_finished)
        self.get_login_password(self.correct_pub)
        self.stop_auto_correction_op_button.setEnabled(True)

    def ignore_pub(self):
        """
        TODO
        """
        rows_done = []
        for selected_item in self.auto_correcttion_table.selectedItems():
            row = selected_item.row()
            if row not in rows_done:
                rows_done.append(row)
                reatrib = Reatribution.get_or_none(
                    (Reatribution.start_element == int(self.auto_correcttion_table.item(row, 4).text())) &
                    (Reatribution.end_element == int(self.auto_correcttion_table.item(row, 5).text()))
                )
                if reatrib is not None:
                    correction_pub = CorrectionPub(
                        docid=self.auto_correcttion_table.item(row, 0).text(),
                        reatribution=reatrib,
                        publication_date=self.auto_correcttion_table.item(row, 2).text(),
                        url=self.auto_correcttion_table.item(row, 3).text(),
                        hal_id=self.auto_correcttion_table.item(row, 6).text(),
                        status='ignorer manuellement',
                        ignored=True,
                    )
                    correction_pub.save()


    def correct_selected_pub(self):
        if self._options.instance is None:
            QMessageBox.warning(self, "Pas de protail selectionner", "Vous devez allez dans préférence pour choisir un portail.")
            return
        else:
            self._correct_doc_thread = CorrectDocThread(self._options.instance.code)
        self.auto_correction_current_op_label.setText('Opération courante : Corriger les publication selectionner')
        rows = []
        for selected_item in self.auto_correcttion_table.selectedItems():
            row = selected_item.row()
            if row not in rows:
                rows.append(row)
        self.add_task_to_correct_doc(rows)
        self._correct_doc_thread.progress_sig.connect(self.correct_selected_pub_progress)
        self._correct_doc_thread.finished.connect(self.correct_selected_pub_finished)
        self.get_login_password(self.correct_pub)
        self.stop_auto_correction_op_button.setEnabled(True)

    def correct_pub(self, login, password):
        if self._password_widget.password_saved:
            self._correct_doc_thread.start(login, password)
        else:
            self._correct_doc_thread.start(login)

    def correct_selected_pub_progress(self, precent_progress, datas):
        self.auto_correction_progressbar.setValue(precent_progress)
        ignored=True
        if not datas[4]:
            message = 'pas d\'affiliation dirrecte'
        else:
            if datas[3].status_code == 200:
                message = 'modification effectuer'
            elif datas[3].status_code == 401:
                message = 'problème d\'identification'
                ignored=False
            else:
                message = 'erreur inconnue'
        reatrib = Reatribution.get_or_none((Reatribution.start_element == int(datas[1])) & (Reatribution.end_element == int(datas[2])))
        pub_date = self.auto_correcttion_table.item(datas[5], 2).text()
        url_s = self.auto_correcttion_table.item(datas[5], 3).text()
        hal_id = self.auto_correcttion_table.item(datas[5], 6).text()
        (
            CorrectionPub
            .replace(docid=datas[0], reatribution=reatrib, publication_date=pub_date, url=url_s, status=message, ignored=ignored, hal_id=hal_id)
            .execute()
        )
        self.auto_correcttion_table.item(datas[5], 7).setText(message)

    def correct_selected_pub_finished(self):
        self.auto_correction_current_op_label.setText('Opération courante : Aucune')
        self.auto_correction_progressbar.setValue(0)
        self.stop_auto_correction_op_button.setEnabled(False)

    def get_login_password(self, callback):
        self._password_widget = PasswordWidget(self._options)
        self._password_widget.validated.connect(callback)
        self._password_widget.show()

    def filter_auto_correcttion(self, row):
        if not self.show_ignored_auto_corr_checkbox.isChecked():
            if self.auto_correcttion_table.item(row, 8).text() == 'Oui':
                return True
            else:
                return False

    def error_auto_correcttion(self):
        self.search_in_table(self.auto_correcttion_table, self.auto_correcttion_combo, self.auto_correcttion_search_line, filters=[self.filter_auto_correcttion])

    @check_db
    def find_auto_correction(self):
        self.find_auto_correction_button.setEnabled(False)
        self.auto_correction_current_op_label.setText('Opération courante : Recherche publication')
        self._error_finding_thread = ErrorFindingThread('find_auto_correction')
        reatributions = []
        for reatribution in Reatribution.select():
            struct = StructureHal.get_or_none(StructureHal.docid == reatribution.start_element)
            if struct is not None and struct.ignore.count() != 0:
                continue
            struct = StructureHal.get_or_none(StructureHal.docid == reatribution.end_element)
            if struct is not None and struct.ignore.count() != 0:
                continue
            if reatribution.ambiguities.get_or_none() is None:
                reatributions.append(reatribution)
        self._error_finding_thread.set_reatributions(reatributions)
        self._error_finding_thread.progress_sig.connect(self.add_auto_correction)
        self._error_finding_thread.new_element.connect(self.new_auto_correction)
        self._error_finding_thread.finished.connect(self.find_auto_correction_finished)
        self._error_finding_thread.start()
        self.stop_auto_correction_op_button.setEnabled(True)

    def stop_auto_correction_op(self):
        if self._error_finding_thread is not None:
            self._error_finding_thread.cancel()
        if self._correct_doc_thread is not None:
            self._correct_doc_thread.cancel()

    def find_auto_correction_finished(self):
        self.auto_correction_current_op_label.setText('Opération courante : Aucune')
        self.auto_correction_progressbar.setValue(0)
        self.find_auto_correction_button.setEnabled(True)
        self.stop_auto_correction_op_button.setEnabled(False)

    def add_auto_correction(self, precent_progress):
        self.auto_correction_progressbar.setValue(precent_progress)

    def is_child_affiliate(self, id_struct, affiliations:list):
        return str(id_struct) not in {re.search("_JoinSep_([0-9]*)_FacetSep_", affiliation).group()[9:-10] for affiliation in affiliations}

    def new_auto_correction(self, new_line: list):
        start_struct_id = int(new_line[4])
        correction = CorrectionPub.get_or_none(CorrectionPub.docid == int(new_line[0]))
        if correction and int(correction.reatribution.start_element) == start_struct_id:
            return
        if self.is_child_affiliate(start_struct_id, new_line.pop()):
            message = 'pas d\'affiliation dirrecte'
            reatrib = Reatribution.get_or_none((Reatribution.start_element == start_struct_id) & (Reatribution.end_element == int(new_line[5])))
            pub_date = new_line[2]
            url_s = new_line[3]
            hal_id = new_line[6]
            (
                CorrectionPub
                .replace(docid=new_line[0], reatribution=reatrib, publication_date=pub_date, url=url_s, status=message, ignored=True, hal_id=hal_id)
                .execute()
            )
            new_line.append('Oui')
        else:
            new_line.append('Non')
        self.add_row_to_table(new_line, self.auto_correcttion_table, editable=True)
        self.search_in_table(self.auto_correcttion_table, self.auto_correcttion_combo, self.auto_correcttion_search_line, filters=[self.filter_auto_correcttion], rows=[self.auto_correcttion_table.rowCount()-1])

    def import_root(self):
        roots_file_name, _ = QFileDialog.getOpenFileName(
            self,
            'Ouvrir liste sutructure racines',
            str(pathlib.Path.home()),
            'liste json (*.json)'
        )
        if roots_file_name != '':
            with open(roots_file_name, 'r') as roots_file:
                json_object = json.load(roots_file)
            for docid in json_object:
                self._controler.add_struct(docid)

    def open_aurehal(self, table):
        def slot():
            if table.currentRow() >= 0:
                row = table.currentRow()
                docid = table.item(row, 0).text()
                webbrowser.open('https://aurehal.archives-ouvertes.fr/structure/read/id/' + docid)
            else:
                QMessageBox.warning(self, "Pas de structure séléctionner", "Pas de structure séléctionner.")
        return slot

    def element_without_struct(self):
        def new_filter(row):
            elements = self.line_table.item(row, 3).text()
            for element in elements.split(','):
                structure_hal = StructureHal.get_or_none(StructureHal.docid == int(element))
                if structure_hal is None:
                    return False
            return True
        if self.element_without_struct_checkbox.isChecked():
            self.search_in_table(self.line_table, self.line_search_combo, self.line_search_line, filters=[new_filter])
        else:
            self.search_in_table(self.line_table, self.line_search_combo, self.line_search_line)

    def structure_hal_right_click(self, position):
        """
        Création et affichage du menu contextuel pour la table structure hal.
        """
        row = self.structure_hal_table.currentRow()
        if self.structure_hal_table.item(row, 0) is not None:
            struct_menu = QMenu(self)
            self.new_struct_hal_select(row)
            delete_menu = struct_menu.addAction("Supprimer")
            delete_menu.triggered.connect(self.del_selected_struct)
            update_menu = struct_menu.addAction("Mise à jour")
            update_menu.triggered.connect(self.update_selected_struct)
            struct_menu.addSeparator()
            find_menu = struct_menu.addAction("Chercher")
            find_menu.triggered.connect(self.context_search(self.structure_hal_table, self.hal_search_combo, self.hal_search_line))
            struct_menu.exec(self.structure_hal_table.mapToGlobal(position))

    def line_table_right_click(self, position):
        """
        Création et affichage du menu contextuel pour la table line.
        """
        line_menu = QMenu(self)
        if self.line_table.currentItem() is not None:
            update_menu = line_menu.addAction("Chercher")
            update_menu.triggered.connect(self.context_search(self.line_table, self.line_search_combo, self.line_search_line))
            line_menu.addSeparator()
        delete_menu = line_menu.addAction("Supprimer")
        delete_menu.triggered.connect(self.del_line)
        line_menu.exec(self.line_table.mapToGlobal(position))

    def element_table_right_click(self, position):
        if self.element_table.currentItem() is not None:
            element_menu = QMenu(self)
            update_menu = element_menu.addAction("Chercher")
            update_menu.triggered.connect(self.context_search(self.element_table, self.element_search_combo, self.element_search_line))
            element_menu.exec(self.element_table.mapToGlobal(position))

    def context_search(self, table:QTableWidget, combo:QComboBox, line:QLineEdit):
        def context_slot():
            current_item = table.currentItem()
            if current_item is not None:
                combo.setCurrentIndex(current_item.column()+1)
                line.setText(current_item.text())
        return context_slot

    def search_in_table(self, table, combo, line, indexes=None, filters: list=[], rows: list|None=None):
        self.activate_same_search()
        try:
            rows = range(table.rowCount()) if rows is None else rows
        except AttributeError:
            rows = range(table.model().rowCount()) if rows is None else rows
        apply_filters = filters.copy()
        apply_filters.append(make_filer_line(table, combo, line, indexes))
        for row in rows:
            table.showRow(row)
            for apply_filter in apply_filters:
                if apply_filter(row):
                    table.hideRow(row)
                    break

    def activate_same_search(self, same_seearch_widget:SameSearchTableWidget=None):
        if self._same_search_table_activated is not None:
            self._same_search_table_activated.unsearch()
        self._same_search_table_activated = same_seearch_widget


    # event database implementation
    def db_new_line(self, new_line: Line):
        elements = ', '.join([str(element.hal_id) for element in new_line.elements])
        line_data = [
            new_line.label,
            new_line.acronym,
            new_line.type,
            elements,
        ]
        self.add_row_to_table(line_data, self.line_table, new_line.id)
        self.search_in_table(self.line_table, self.line_search_combo, self.line_search_line, rows=[self.line_table.rowCount() - 1])

    def db_update_line(self, updated_line: Line):
        elements = ', '.join([str(element.hal_id) for element in updated_line.elements])
        line_data = [
            updated_line.label,
            updated_line.acronym,
            updated_line.type,
            elements,
        ]
        row = self._ids2rows[self.line_table.objectName()][updated_line.id]
        self.update_row_table(line_data, self.line_table, row)
        self.search_in_table(self.line_table, self.line_search_combo, self.line_search_line, rows=[row])

    def db_new_structure_ignore(self, structure_ignore:StructureIgnore):
        row = self._ids2rows[self.structure_hal_table.objectName()][structure_ignore.structure.id]
        self._ignore_struct[structure_ignore.id] = structure_ignore.structure.id
        self.structure_hal_table.item(row, 12).setText(structure_ignore.message)
        self.structure_hal_table.hideRow(row)
        self.ignored_struct_table_view.showRow(row)

    def db_new_structure_task(self, structure_task:StructureTask):
        if not structure_task.done:
            structure = structure_task.structure
            if not structure_task.done:
                self.delete_row_table(self.structure_hal_table, row_id=structure.id)
                self.add_row_to_table([
                    structure.docid,
                    structure.acronym_s,
                    structure.label_s,
                    structure.valid_s,
                    str(len(StructureTask.select().where((StructureTask.structure == structure) & (StructureTask.done == False)))) +
                    '/' + str(len(StructureTask.select().where(StructureTask.structure == structure)))
                ], self.structure_task_table, structure.id)

    def db_update_structure_task(self, structure_task:StructureTask):
        for current_id in range(self.task_combo.count()):
            if self.task_combo.itemData(current_id, 0X0100) == structure_task.id:
                self.task_combo.setItemText(current_id, structure_task.message)
        self.recalculate_task_undone_count(structure_task.structure)

    def recalculate_task_undone_count(self, structure):
        task_undone_count = len(StructureTask.select().where((StructureTask.structure == structure) & (StructureTask.done == False)))
        if task_undone_count == 0:
            self.delete_row_table(self.structure_task_table, row_id=structure.id)
            self.db_new_structure_hal(structure)
        else:
            self.update_row_table([
                structure.docid,
                structure.acronym_s,
                structure.label_s,
                structure.valid_s,
                str(task_undone_count) + '/' + str(len(StructureTask.select().where(StructureTask.structure == structure)))
            ], self.structure_task_table, row_id=structure.id)

    def db_delete_structure_ignore(self, structure_ignore_id):
        """
        db_delete_event: StructureIgnore;
        """
        row = self._ids2rows['structure_hal_table'][self._ignore_struct[structure_ignore_id]]
        self.ignored_struct_table_view.hideRow(row)
        self.structure_hal_table.showRow(row)

    def db_delete_structure_task(self, structure_task_id):
        """
        db_delete_event: StructureTask;
        """
        if self.task_combo.currentData(0x0100) == structure_task_id:
            self.task_combo.removeItem(self.task_combo.currentIndex())
            self.task_details_text.setPlainText('')
            self.task_done_check.setChecked(False)
        else:
            for current_id in range(self.task_combo.count()):
                if self.task_combo.itemData(current_id, 0X0100) == structure_task_id:
                    self.task_combo.removeItem(current_id)
        structure = StructureHal.get_or_none(StructureHal.id == self.structure_task_table.currentItem().data(0x0100))
        if structure is not None:
            self.recalculate_task_undone_count(structure)

    def db_new_reatrib(self, reatrib: Reatribution):
        period = reatrib.period
        self.add_row_to_table([
            reatrib.name,
            period,
            reatrib.start_element,
            reatrib.end_element,
            'oui' if reatrib.ambiguities.count() > 0 else 'non',
        ], self.reatribute_table, reatrib.id)
        if period not in self._periods:
            self.reatribute_period_combo.addItem(period)
            self._periods.append(period)
        self.search_in_table(self.reatribute_table, self.reatrib_search_combo, self.reatrib_search_line, [0, 2, 3], filters=[self.filter_reatrib_amb(), self.filter_reatrib_period()], rows=[self.reatribute_table.rowCount() - 1])

    def db_update_reatrib(self, reatrib: Reatribution):
        period = reatrib.period
        row_data = [
            reatrib.name,
            period,
            reatrib.start_element,
            reatrib.end_element,
        ]
        if period not in self._periods:
            self.reatribute_period_combo.addItem(period)
            self._periods.append(period)
        self.update_row_table(row_data, self.reatribute_table, row_id=reatrib.id)

    def db_new_structure_hal(self, structure_hal: StructureHal):

        def search_acronym(text):
            self.hal_search_line.setText(text)
            self.hal_search_combo.setCurrentIndex(3)

        def unsearch():
            self.hal_search_line.setText('')
            self.hal_search_combo.setCurrentIndex(0)

        acronym_s = structure_hal.acronym_s
        ignore_message = ''
        if structure_hal.ignore.count() != 0:
            self._ignore_struct[structure_hal.ignore.get().id] = structure_hal.id
            ignore_message = structure_hal.ignore.get().message
        row_data = [
            structure_hal.docid,
            structure_hal.last_update,
            SameSearchTableWidget(acronym_s, search_acronym, unsearch, self),
            acronym_s,
            structure_hal.label_s,
            structure_hal.valid_s,
            structure_hal.type_s,
            structure_hal.address_s,
            structure_hal.url_s,
            structure_hal.endDate_s,
            structure_hal.startDate_s,
            structure_hal.diff_element,
            ignore_message,
        ]
        self.add_row_to_table(row_data, self.structure_hal_table, structure_hal.id)
        self.refresh_structure_hal_table([self.structure_hal_table.rowCount() - 1])

    def db_update_structure_hal(self, structure_hal: StructureHal):
        acronym_s = structure_hal.acronym_s
        row_data = [
            structure_hal.docid,
            structure_hal.last_update,
            acronym_s,
            acronym_s,
            structure_hal.label_s,
            structure_hal.valid_s,
            structure_hal.type_s,
            structure_hal.address_s,
            structure_hal.url_s,
            structure_hal.endDate_s,
            structure_hal.startDate_s,
            structure_hal.diff_element,
            '' if structure_hal.ignore.count() == 0 else structure_hal.ignore.get().message,
        ]
        self.update_row_table(row_data, self.structure_hal_table, row_id=structure_hal.id)
        self.refresh_structure_hal_table([self.structure_hal_table.rowCount() - 1])

    def db_delete_structure_hal(self, structure_hal_id):
        """
        db_delete_event: StructureHal;
        """
        self.delete_row_table(self.structure_hal_table, row_id=structure_hal_id)

    def db_delete_line(self, line_id):
        """
        db_delete_event: Line;
        """
        self.delete_row_table(self.line_table, row_id=line_id)

    def db_delete_element(self, element_id):
        """
        db_delete_event: Element;
        """
        self.delete_row_table(self.element_table, row_id=element_id)

    def db_update_element(self, element: Element):
        if Element.select().where(Element.recalculate_need == True):
            self.warning_label.setVisible(True)
            self.warning_ico_label.setVisible(True)
            self.warning_label_2.setVisible(True)
            self.warning_ico_label_2.setVisible(True)

        if self.structure_hal_table.currentItem() is not None:
            self.refresh_structure_hal_table([self.structure_hal_table.currentItem().row()])
        element_data = [
            element.hal_id,
            element.date_start,
            element.date_end,
            ', '.join([line.acronym for line in element.lines])
        ]
        structura_hal = StructureHal.get_or_none(StructureHal.docid == element.hal_id)
        if structura_hal is not None:
            self.db_update_structure_hal(structura_hal)
        self.update_row_table(element_data, self.element_table, row_id=element.id)

    def db_new_element(self, element: Element, for_loading: bool=False):
        element_data = [
            element.hal_id,
            element.date_start,
            element.date_end,
            ', '.join([line.acronym for line in element.lines])
        ]
        structura_hal = StructureHal.get_or_none(StructureHal.docid == element.hal_id)
        if structura_hal is not None:
            self.db_update_structure_hal(structura_hal)
        self.add_row_to_table(element_data, self.element_table, element.id)
        self.search_in_table(self.element_table, self.element_search_combo, self.element_search_line, rows=[self.element_table.rowCount() - 1], filters=[self.filter_el_without_stuct])
        if not for_loading:
            self.warning_label.setVisible(True)
            self.warning_ico_label.setVisible(True)
            self.warning_label_2.setVisible(True)
            self.warning_ico_label_2.setVisible(True)

    def db_new_error(self, error: Error):
        if error.error_valut == 0 and error.class_name == Element.__name__:
            self._no_element_hal_id.append(error.instance_id)
        if error.error_valut == 0 and error.class_name == StructureHal.__name__:
            self._no_struct_id.append(error.instance_id)

    # deffinition recherche

    def clean_serch(self, line, combo, search):
        def clean():
            self.activate_same_search()
            line.setText('')
            combo.setCurrentIndex(0)
            search()
        return clean

    def hal_struct_hal_search(self):
        self.search_in_table(self.struct_line_table_view, self.hal_struct_search_combo, self.hal_struct_search_line)

    def error_search(self):
        self.search_in_table(self.pub_error_table, self.error_search_combo, self.error_search_lline)

    def time_end_search(self):
        self.search_in_table(self.pub_time_end_table, self.time_end_search_combo, self.time_end_search_lline)

    def structure_task_search(self):
        self.search_in_table(self.structure_task_table, self.structure_task_search_combo, self.structure_task_search_line)

    def ignored_struct_search(self):
        self.search_in_table(self.ignored_struct_table_view, self.ignored_struct_search_combo, self.ignored_struct_search_line, filters=[lambda row: False if (self.structure_hal_table.item(row, 12).text() != '') else True])

    def reatrib_search(self):
        self.search_in_table(self.reatribute_table, self.reatrib_search_combo, self.reatrib_search_line, [0, 2, 3], filters=[self.filter_reatrib_amb(), self.filter_reatrib_period()])

    def element_search(self):
        self.search_in_table(self.element_table, self.element_search_combo, self.element_search_line, filters=[self.filter_el_without_stuct])

    def line_search(self):
        self.search_in_table(self.line_table, self.line_search_combo, self.line_search_line)

    def help_index(self):
        webbrowser.open(str(app_dir / 'doc\\build\\html\\index.html'))

    def clean_reatrib_table(self):
        self.reatribute_table.setRowCount(0)

    def open_struct_url(self, row):
        self.open_aurehal(self.structure_hal_table)()

    def open_pub_error_url(self, row):
        if self.pub_error_table.item(row, 2) is not None:
            url = self.pub_error_table.item(row, 2).text()
            webbrowser.open(url)

    def import_xlsx(self):
        db_filename, _ = QFileDialog.getSaveFileName(
            self,
            'Ouvrir une base de donnée',
            str(pathlib.Path.home()),
            'base SQLite (*.db)'
        )
        xlsx_filename, _ = QFileDialog.getOpenFileName(
            self,
            'Ouvrir une base de donnée',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if xlsx_filename!='' and  db_filename!='':
            self._controler.generate_db_from_historic_workbook(xlsx_filename, db_filename)

    def stop_hal_op(self):
        if self._query_hal_thread is not None:
            self._query_hal_thread.cancel()
            self._skip_search_new = True

    def stop_error_op(self):
        if self._error_finding_thread is not None:
            self._error_finding_thread.cancel()

    def stop_time_end_op(self):
        if self._time_end_finding_thread is not None:
            self._time_end_finding_thread.cancel()

    def create_database(self):
        filename, format = QFileDialog.getSaveFileName(
            self,
            'Ouvrir une base de donnée',
            str(pathlib.Path.home()),
            'base SQLite (*.db)'
        )
        if format == 'base SQLite (*.db)' and filename!='':
            self.reatribute_table.setRowCount(0)
            self.line_table.setRowCount(0)
            self.structure_hal_table.setRowCount(0)
            self.element_table.setRowCount(0)
            self._change_rows = []
            self._controler.load_database(filename)
            self.warning_label.setVisible(True)
            self.warning_ico_label.setVisible(True)
            self.warning_label_2.setVisible(True)
            self.warning_ico_label_2.setVisible(True)

    @check_db
    def add_struct(self):
        docid, ok = QInputDialog.getInt(self, 'Hal id de la structure à ajouter.', 'Hal id de la structure à ajouter.')
        if ok:
            if StructureHal.get_or_none(StructureHal.docid == docid) is None:
                self._controler.add_struct(docid)
            else:
                QMessageBox.warning(self, "Déjà présente", "la structure demandé est déjà présente.")

    def update_selected_struct(self):
        if self.structure_hal_table.currentItem() is not None:
            row = self.structure_hal_table.currentItem().row()
            id_item = self.structure_hal_table.item(row, 0).data(0x0100)
            structure_hal = StructureHal.get(StructureHal.id == id_item)
            if self._query_hal_thread is not None:
                if not self._query_hal_thread.isFinished():
                    QMessageBox.warning(self, "La tache précédente n'est pas fini", "La tache précédente n'est pas fini")
                    return
            self.structure_hal_current_op_label.setText('Opération courante : Mise à jour')
            self._query_hal_thread = UpdateThread({structure_hal.docid})
            self._query_hal_thread.new_not_found.connect(self.new_struct_not_found)
            self._query_hal_thread.progress_sig.connect(self.update_query_progress)
            self._query_hal_thread.new_struct_sig.connect(self.new_struct)
            self._query_hal_thread.finished.connect(self.update_query_finished)
            self._query_hal_thread.start()
            self.stop_hal_op_button.setEnabled(True)

    def new_struct_not_found(self, docid):
        element = Element.get_or_none(Element.hal_id == docid)
        if element is not None:
            error = Error(error_valut=0)
            error.object_instance = element
            error.save()
        struct = StructureHal.get_or_none(StructureHal.docid == docid)
        if struct is not None:
            error = Error(error_valut=0)
            error.object_instance = struct
            error.save()

    def ignore_export_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrement fichier xslx',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            headers = ['docid', 'Acronyme', 'Label', 'Valid', 'Type', 'Message']
            def select_row(row):
                return self.structure_hal_table.item(row, 12).text() != ''
            export_table_to_xlsx(self.structure_hal_table, filename, headers, select_row=select_row, hide_col=[1, 2, 7, 8, 9, 10, 11])

    def line_export_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrement fichier xslx',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            def add_date_element(elements: str):
                element_list = elements.split(', ')
                result = ''
                for i, element in enumerate(element_list):
                    if i != 0: result += ', '
                    result += element + ' ('
                    struct = StructureHal.get_or_none(StructureHal.docid == element)
                    if struct is not None:
                        result += date_to_string(struct.startDate_s) + ' : '
                        result += date_to_string(struct.endDate_s, True)
                    result += ')'
                return result
            headers = ['Label', 'Acronyme', 'Type', 'Eléments']
            export_table_to_xlsx(self.line_table, filename, headers, {3: add_date_element})

    def element_export_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrement fichier xslx',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            headers = ['hal id', 'date de début', 'date de fin', 'structures']
            export_table_to_xlsx(self.element_table, filename, headers)

    def export_reatrib_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrement fichier xslx',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            headers = ['Nom', 'période', 'line de départ', 'line d''arriver']
            export_table_to_xlsx(self.reatribute_table, filename, headers)

    def export_auto_correcttion_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrement fichier xslx',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            headers = ['Doc id', 'Nom', 'Date de publication', 'URL', 'Modifier',
                       'Forme de départ', 'Forme d\'arriver', 'Suppression seulement'
                       ]
            export_table_to_xlsx(self.auto_correcttion_table, filename, headers)

    def export_structure_hal_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrement fichier xslx',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            headers = ['Doc id', 'Dernière MàJ', 'Acronyme', 'Label', 'Validité',
                       'type', 'adresse', 'URL', 'Date de fin', 'date de débt']
            export_table_to_xlsx(self.structure_hal_table, filename, headers, hide_col=[2, 11])

    @check_db
    def gen_all_reatrib(self):
        image_filename, image_type = QFileDialog.getSaveFileName(
            self,
            'Ouvrir une base de donnée',
            str(pathlib.Path.home()),
            'svg (*.svg);;png (*.png);;jpg (*.jpg);;Graphviz (*.dot)'
        )
        if image_filename != '':
            format_value = {
                'svg (*.svg)': 'svg',
                'png (*.png)': 'png',
                'jpg (*.jpg)': 'jpg',
                'Graphviz (*.dot)': 'dot'
            }[image_type]
            reatrib_done = set()
            for number, reatrib in enumerate(Reatribution.select()):
                reatribution_graph = ReatribtionGraph(reatrib)
                if reatrib.id not in reatrib_done:
                    reatribution_graph.save_img(image_filename[:-4] + '_%s.'%number + format_value, format_value)
                reatrib_done.update(reatribution_graph.reatributions_ids)

    def filter_el_without_stuct(self, row):
        if (not self.without_stuct_check.isChecked()) or (self.element_table.item(row, 0).data(0x0100) in self._no_element_hal_id):
            return False
        return True

    def filter_struct_not_hal(self, row):
        if (not (self.show_struct_mod_combo.currentIndex() == 3)) or (self.structure_hal_table.item(row, 0).data(0x0100) in self._no_struct_id):
            return False
        return True

    def filter_reatrib_amb(self):
        if self.show_all_reatrib_radio.isChecked():
            new_state = 0
        elif self.show_no_amb_reatrib_radio.isChecked():
            new_state = 1
        elif self.show_amb_reatrib_radio.isChecked():
            new_state = 2
        def new_filter(row):
            if new_state == 2:
                if self.reatribute_table.item(row, 4).text() == 'non':
                    return True
            elif new_state == 1:
                if self.reatribute_table.item(row, 4).text() == 'oui':
                    return True
            return False
        return new_filter

    def show_reatrib_amb(self):
        self.search_in_table(self.reatribute_table, self.reatrib_search_combo, self.reatrib_search_line, [0, 2, 3], filters=[self.filter_reatrib_amb(), self.filter_reatrib_period()])

    @check_db
    def find_ambiguity_pub(self):
            self._error_finding_thread = ErrorFindingThread('ambiguity', self._options.start_year)
            reatributions = []
            for ambiguity in Ambiguity.select():
                struct = StructureHal.get_or_none(StructureHal.docid == ambiguity.start_element.hal_id)
                if struct is not None and struct.ignore.count() != 0:
                    continue
                reatributions.append([reatribution for reatribution in ambiguity.reatributions])
            self._error_finding_thread.set_reatributions(reatributions)
            self._error_finding_thread.progress_sig.connect(self.update_find_error)
            self._error_finding_thread.new_element.connect(self.new_element_error)
            self._error_finding_thread.finished.connect(self.find_error_finished)
            self._error_finding_thread.start()
            self.error_finding_current_op_label.setText('Opération courante : Rechercher les publication avec réatribtion ambigüe')
            self.stop_error_op_button.setEnabled(True)

    def create_new_line_from_hal(self):
        response = QMessageBox.question(self, 'Création d\'une nouvelle ligne', 'Êtes-vous sure de vouloire créé une nouvelle ligne ?')
        if response == QMessageBox.StandardButton.Yes.value and self.structure_hal_table.currentItem() is not None:
            row = self.structure_hal_table.currentItem().row()
            id_item = self.structure_hal_table.item(row, 0).data(0x0100)
            structure_hal = StructureHal.get(StructureHal.id == id_item)
            element = Element.get_or_none(Element.hal_id == structure_hal.docid)
            if element is None:
                element = Element(
                    hal_id = structure_hal.docid,
                    date_start = structure_hal.startDate_s,
                    date_end = structure_hal.endDate_s,
                )
                element.save()
            new_line = Line(
                label = structure_hal.label_s if structure_hal.label_s is not None else '',
                acronym = structure_hal.acronym_s if structure_hal.acronym_s is not None else '',
                type = structure_hal.type_s,
            )
            new_line.save()
            new_line.elements.add(element)
            new_line.save()

    def gen_struct_graph(self):
        if self.line_table.currentItem() is not None:
            image_filename, image_type = QFileDialog.getSaveFileName(
                self,
                'Ouvrir une base de donnée',
                str(pathlib.Path.home()),
                'svg (*.svg);;png (*.png);;jpg (*.jpg);;Graphviz (*.dot)'
            )
            if image_filename != '':
                row = self.line_table.currentItem().row()
                id_item = self.line_table.item(row, 0).data(0x0100)
                reatrib = Line.get(Line.id == id_item)
                reatribution_graph = StructbWithReatrib(reatrib)
                format_value = {
                    'svg (*.svg)': 'svg',
                    'png (*.png)': 'png',
                    'jpg (*.jpg)': 'jpg',
                    'Graphviz (*.dot)': 'dot'
                }
                reatribution_graph.save_img(image_filename, format_value[image_type])

    def filter_structur_hal(self):
        validity_list = [
            validity_checkbox.text()
            for validity_checkbox in [
                self.show_valid_checkbox,
                self.show_old_checkbox,
            ]
            if validity_checkbox.isChecked()
        ]
        def new_filter(row):
            if self.filter_struct_not_hal(row):
                return True
            if self.structure_hal_table.item(row, 12).text() != '':
                return True
            else:
                self.ignored_struct_table_view.hideRow(row)
            item_validity = self.structure_hal_table.item(row, 5)
            diff_element = self.structure_hal_table.item(row, 11).text()
            if self.only_root_struct_checkbox.isChecked():
                if int(self.structure_hal_table.item(row, 0).text()) not in list(StructureHal.get_roots()):
                    return True
            if item_validity.text() not in validity_list:
                return True
            if self.show_struct_mod_combo.currentIndex() == 1:
                if diff_element != StructureHal.ELEMENT_SYNC_STATUS[1]:
                    return True
            if self.show_struct_mod_combo.currentIndex() == 2:
                if diff_element != StructureHal.ELEMENT_SYNC_STATUS[0]:
                    return True
        return new_filter

    def refresh_structure_hal_table(self, row=None):
        if type(row) != list:
            row=None
        self.search_in_table(self.structure_hal_table, self.hal_search_combo, self.hal_search_line, filters=[self.filter_structur_hal()], rows=row)

    def new_element_select(self):
        for _ in range(self.element_line_toolbox.count()):
            self.element_line_toolbox.removeItem(0)
        if self.element_table.currentItem() is not None:
            row = self.element_table.currentItem().row()
            id_item = self.element_table.item(row, 0).data(0x0100)
            for line in Element.get(Element.id == id_item).lines:
                self.element_line_toolbox.addItem(LineWidget(line), line.acronym)

    @check_db
    def get_child_lines(self):
        """
        Recherche novuelle StructurHal
        """
        self.update_queries_button.setEnabled(False)
        structure_hal_list = {structure_hal.docid for structure_hal in StructureHal.select()}
        self.structure_hal_current_op_label.setText('Opération courante : Recherche nouvelles structures')
        self._query_hal_thread = UpdateThread(structure_hal_list, True)
        self._query_hal_thread.progress_sig.connect(self.update_query_progress)
        self._query_hal_thread.new_struct_sig.connect(self.new_struct)
        self._query_hal_thread.finished.connect(self.update_query_finished)
        self._query_hal_thread.start()
        self.stop_hal_op_button.setEnabled(True)

    def new_struct_hal_select(self, row):
        layout = self.structure_hal_line_widget.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)
        self.hal_create_struct_button.setEnabled(True)
        id_item = self.structure_hal_table.item(row, 0).data(0x0100)
        structure_hal = StructureHal.get_or_none(StructureHal.id == id_item)
        if structure_hal is not None:
            if structure_hal.startDate_s:
                self.startDate_label.setText('startDate_s : ' + str(structure_hal.startDate_s))
            else:
                self.startDate_label.setText('startDate_s : ')
            if structure_hal.endDate_s:
                self.endDate_label.setText('endDate_s : ' + str(structure_hal.endDate_s))
            else:
                self.endDate_label.setText('endDate_s : ')
            layout = self.structure_hal_element_widget.layout()
            for i in reversed(range(layout.count())):
                layout.itemAt(i).widget().setParent(None)
            element_selected = Element.get_or_none(Element.hal_id == structure_hal.docid)
            if element_selected is not None:
                new_element_widget = ElementWidget(element_selected, structure_hal=structure_hal)
                new_element_widget.modified.connect(lambda :self.new_struct_hal_select(row))
                layout.addWidget(new_element_widget)
        self.search_in_table(self.struct_line_table_view, self.hal_struct_search_combo, self.hal_struct_search_line)

    def search_struct(self):
        row = self.structure_hal_table.currentRow()
        if row >= 0:
            id_item = self.structure_hal_table.item(row, 0).data(0x0100)
            structure_hal = StructureHal.get_or_none(StructureHal.id == id_item)
            if structure_hal is not None:
                element_selected = Element.get_or_none(Element.hal_id == structure_hal.docid)
                if element_selected is not None:
                    self.hal_struct_search_line.setText(str(element_selected.hal_id))
                    self.hal_struct_search_combo.setCurrentIndex(4)
                else:
                    self.hal_struct_search_combo.setCurrentIndex(2)
                    self.hal_struct_search_line.setText(self.structure_hal_table.cellWidget(row, 2).text())

    @check_db
    def update_queries(self):
        """
        mise à jour de la table StructurHal
        """
        self.update_queries_button.setEnabled(False)
        element_list = {element.hal_id for element in Element.select(Element.hal_id)}
        element_list = element_list.union({element.docid for element in StructureHal.select(StructureHal.docid)})
        self.structure_hal_current_op_label.setText('Opération courante : Mise à jour')
        self._query_hal_thread = UpdateThread(element_list)
        self._query_hal_thread.new_not_found.connect(self.new_struct_not_found)
        self._query_hal_thread.progress_sig.connect(self.update_query_progress)
        self._query_hal_thread.new_struct_sig.connect(self.new_struct)
        self._query_hal_thread.finished.connect(self.update_only_query_finished)
        self._query_hal_thread.start()
        self.stop_hal_op_button.setEnabled(True)

    def update_query_finished(self):
        self.update_queries_button.setEnabled(True)
        self._skip_search_new = False
        self.structure_hal_update_progressbar.setValue(0)
        self.structure_hal_current_op_label.setText('Opération courante : Aucune')
        self.stop_hal_op_button.setEnabled(False)

    def update_only_query_finished(self):
        self.update_queries_button.setEnabled(True)
        if not self._skip_search_new:
            self.get_child_lines()
        self.update_query_finished()

    def new_struct(self, doc:dict =None, last_update=None):
        self._controler.save_structure_hal_doc(doc, last_update)
    
    def update_query_progress(self, precent_progress):
        self.structure_hal_update_progressbar.setValue(precent_progress)

    def export_erreur_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrement fichier xslx',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            save_wb = Workbook()
            sheet = save_wb.active
            sheet['A1'] = 'doc id'
            sheet['B1'] = 'date de publication'
            sheet['C1'] = 'doc url'
            sheet['D1'] = 'Hal id source'
            sheet['E1'] = 'Ambiguité Hal id'
            line = 2
            for doc_error in self._doc_error:
                sheet['A' + str(line)] = doc_error[0]
                sheet['B' + str(line)] = doc_error[1]
                sheet['C' + str(line)] = doc_error[2]
                sheet['D' + str(line)] = doc_error[3]
                sheet['E' + str(line)] = doc_error[4]
                line += 1
            save_wb.save(filename=filename)

    def export_time_end_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Enregistrement fichier xslx',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            save_wb = Workbook()
            sheet = save_wb.active
            sheet['A1'] = 'doc id'
            sheet['B1'] = 'date de publication'
            sheet['C1'] = 'doc url'
            sheet['D1'] = 'id line'
            sheet['E1'] = 'date fin line'
            line = 2
            for doc_time_end in self._doc_time_end:
                sheet['A' + str(line)] = doc_time_end[0]
                sheet['B' + str(line)] = doc_time_end[1]
                sheet['C' + str(line)] = doc_time_end[2]
                sheet['D' + str(line)] = doc_time_end[3]
                sheet['E' + str(line)] = doc_time_end[4]
                line += 1
            save_wb.save(filename=filename)

    @check_db
    def find_time_end_pub(self):
        '''
        recherche les publication arriver après la fermeture d'une line
        '''
        self._time_end_finding_thread = ErrorFindingThread('time_end', self._options.start_year)
        end_elements = set()
        for line in Line.select():
            end_element = line.elements.order_by(Element.date_start, -Element.date_end)[-1]
            if True in [other_line.elements.order_by(Element.date_start, -Element.date_end)[-1] != end_element
                        for other_line in end_element.lines]:
                continue
            end_elements.add(end_element)
        self._time_end_finding_thread.set_end_elements(end_elements)
        self._time_end_finding_thread.progress_sig.connect(self.update_find_time_end)
        self._time_end_finding_thread.new_element.connect(self.new_element_time_end)
        self._time_end_finding_thread.finished.connect(self.find_time_end_finished)
        self._time_end_finding_thread.start()
        self.time_end_finding_current_op_label.setText('Opération courante : Recherche publication après la fin')
        self.stop_time_end_op_button.setEnabled(True)

    def find_error_finished(self):
        self.error_finding_current_op_label.setText('Opération courante : Aucune')
        self.find_error_progressbar.setValue(0)
        self.stop_error_op_button.setEnabled(False)

    def find_time_end_finished(self):
        self.time_end_finding_current_op_label.setText('Opération courante : Aucune')
        self.find_time_end_progressbar.setValue(0)
        self.stop_time_end_op_button.setEnabled(False)

    def update_find_error(self, precent_progress):
        self.find_error_progressbar.setValue(precent_progress)

    def update_find_time_end(self, precent_progress):
        self.find_time_end_progressbar.setValue(precent_progress)

    def new_element_error(self, new_line):
        self.add_row_to_table(new_line, self.pub_error_table)
        self.search_in_table(self.pub_error_table, self.error_search_combo, self.error_search_lline, rows=[self.pub_error_table.rowCount() - 1])
        self._doc_error.append(new_line)

    def new_element_time_end(self, new_line):
        self.add_row_to_table(new_line, self.pub_time_end_table)
        self.search_in_table(self.pub_time_end_table, self.time_end_search_combo, self.time_end_search_lline, rows=[self.pub_time_end_table.rowCount() - 1])
        self._doc_time_end.append(new_line)

    @check_db
    def check_root_struct(self):
        self._root_struct_widget = RootStructWidget(self._controler)
        self._root_struct_widget.show()

    @check_db
    def check_elements_date(self):
        self._inconsistencies_check_widget = InconsistenciesCheckWidget()
        self._inconsistencies_check_widget.show()

    def gen_reatrib_graph(self):
        if self.reatribute_table.currentItem() is not None:
            image_filename, image_type = QFileDialog.getSaveFileName(
                self,
                'Ouvrir une base de donnée',
                str(pathlib.Path.home()),
                'svg (*.svg);;png (*.png);;jpg (*.jpg);;Graphviz (*.dot)'
            )
            if image_filename != '':
                row = self.reatribute_table.currentItem().row()
                id_item = self.reatribute_table.item(row, 0).data(0x0100)
                reatrib = Reatribution.get(Reatribution.id == id_item)
                reatribution_graph = ReatribtionGraph(reatrib)
                format_value = {
                    'svg (*.svg)': 'svg',
                    'png (*.png)': 'png',
                    'jpg (*.jpg)': 'jpg',
                    'Graphviz (*.dot)': 'dot'
                }
                reatribution_graph.save_img(image_filename, format_value[image_type])
        else:
            QMessageBox.warning(self, "Pas de réattribution séléctionner.", "Vous devez d'abord séléction une réattribution dans le tableau.")

    def new_line_select(self):
        layout = self.show_line_widget.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)
        if self.line_table.currentItem() is not None:
            row = self.line_table.currentItem().row()
            id_item = self.line_table.item(row, 0).data(0x0100)
            selected_line = Line.get_or_none(Line.id == id_item)
            if selected_line is None:
                self.delete_row_table(self.line_table, row)
                return
            layout.addWidget(LineWidget(selected_line))

    def del_line(self):
        row = self.line_table.currentRow()
        if row >= 0:
            id_item = self.line_table.item(row, 0).data(0x0100)
            line = Line.get(Line.id == id_item)
            for element in line.elements:
                element.lines.remove(line)
                if len(element.lines) == 0:
                    self._controler.delete_instance(element)
            self._controler.delete_instance(line)

    def del_selected_struct(self):
        row = self.structure_hal_table.currentRow()
        if row >= 0:
            id_item = self.structure_hal_table.item(row, 0).data(0x0100)
            self._controler.delete_instance(StructureHal.get_by_id(id_item))

    def structure_hal_new_struct_select(self):
        layout = self.structure_hal_line_widget.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)
        if self.struct_line_table_view.currentIndex().data() is not None:
            row = self.struct_line_table_view.currentIndex().row()
            id_item = self.struct_line_table_view.model().index(row, 0).data(0x0100)
            selected_line = Line.get(Line.id == id_item)
            if self.structure_hal_table.currentRow() >= 0:
                row = self.structure_hal_table.currentRow()
                id_item = self.structure_hal_table.item(row, 0).data(0x0100)
                selected_structure_hal = StructureHal.get(StructureHal.id == id_item)
                layout.addWidget(LineWidget(selected_line, selected_structure_hal))
            else:
                layout.addWidget(LineWidget(selected_line))

    def filter_reatrib_period(self):
        def new_filter(row):
            if self.reatribute_period_combo.currentText() != 'Toutes':
                if self.reatribute_period_combo.currentText() != self.reatribute_table.item(row, 1).text():
                    return True
            return False
        return new_filter

    def select_period(self):
        self.search_in_table(self.reatribute_table, self.reatrib_search_combo, self.reatrib_search_line, [0, 2, 3], filters=[self.filter_reatrib_amb(), self.filter_reatrib_period()])

    @check_db
    def save_xlsx(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            'Ouvrir une base de donnée',
            str(pathlib.Path.home()),
            'xlsx (*.xlsx)'
        )
        if filename!='':
            self._controler.generate_workbook_from_db(filename)

    def add_correction(self, correction: CorrectionPub):
        new_line = [
            correction.docid,
            correction.reatribution.name,
            correction.publication_date,
            correction.url,
            correction.reatribution.start_element,
            correction.reatribution.end_element,
            correction.hal_id,
            correction.status,
            'Oui' if correction.ignored else 'Non',
        ]
        self.add_row_to_table(new_line, self.auto_correcttion_table, editable=True)

    def reopen_database(self):
        if self._options.last_database != '':
            self._open_databe(self._options.last_database)

    def open_database(self):
        filename, format = QFileDialog.getOpenFileName(
            self,
            'Ouvrir une base de donnée',
            str(pathlib.Path.home()),
            'base SQLite (*.db)'
        )
        if format == 'base SQLite (*.db)' and filename!='':
            self._options.last_database = filename
            self._options.save()
            self._open_databe(filename)

    def _open_databe(self, filename):
        self._db_open_message.setText(self.OPEN_DB_MESSAGE + filename)
        self.backup_db_action.setEnabled(True)
        self.reatribute_table.setRowCount(0)
        self.line_table.setRowCount(0)
        self.structure_hal_table.setRowCount(0)
        self.element_table.setRowCount(0)
        self.structure_task_table.setRowCount(0)
        self.auto_correcttion_table.setRowCount(0)
        self._doc_error = []
        self._doc_time_end = []
        self._query_hal_thread = None
        self._error_finding_thread = None
        self._ids2rows = {}
        self._same_search_table_activated = None
        self._ignore_struct = {}
        self._correct_doc_thread = None
        self._parent_structs_check = {}
        self._no_element_hal_id = []
        self._no_struct_id = []
        self._change_rows = []
        self._periods = []
        self._controler.load_database(filename)
        for reatrib in Reatribution.select():
            self.db_new_reatrib(reatrib)
        self.show_reatrib_amb()
        for line in Line.select():
            self.db_new_line(line)
        for structure_hal in StructureHal.select():
            self.db_new_structure_hal(structure_hal)
            if structure_hal.parentDocid_i is not None:
                for parent in structure_hal.parentDocid_i.split(','):
                    if parent not in self._parent_structs_check.keys():
                        self._parent_structs_check[parent] = StructureHal.select().where(StructureHal.docid == parent).exists()
        for element in Element.select():
            self.db_new_element(element, True)
        if Element.select().where(Element.recalculate_need == True):
            self.warning_label.setVisible(True)
            self.warning_ico_label.setVisible(True)
            self.warning_label_2.setVisible(True)
            self.warning_ico_label_2.setVisible(True)
        for task in StructureTask.select():
            self.db_new_structure_task(task)
        for correction in CorrectionPub.select():
            self.add_correction(correction)
        self.error_auto_correcttion()
        if self._options.autobackup != AutoBackupOption.No:
            self._controler.backup_db(AutoBackupOption.ONE_PER_DAY == self._options.autobackup)
        for error in Error.select():
            self.db_new_error(error)

    def show_option(self):
        self._option_widget = OptionWidget(self._options, self._controler)
        self._option_widget.show()

    def add_row_to_table(self, row_data, table, id=None, editable=False):
        row = table.rowCount()
        table.setRowCount(row+1)
        for col, item in enumerate(row_data):
            if item is None:
                item = ''
            if isinstance(item, QWidget):
                cell = item
                table.setCellWidget(row, col, cell)
            else:
                cell = QTableWidgetItem(str(item))
                if not editable:
                    cell.setFlags(cell.flags() ^ Qt.ItemFlag.ItemIsEditable)
                table.setItem(row, col, cell)
                if id is not None:
                    cell.setData(0x0100, id)
                if table.objectName() not in self._ids2rows:
                    self._ids2rows[table.objectName()] = {}
                self._ids2rows[table.objectName()][id] = row

    def update_row_table(self, row_data, table, row=None, row_id=None):
        if row is None:
            row = self._ids2rows[table.objectName()][row_id]
        for col, item in enumerate(row_data):
            if item is None:
                item = ''
            if table.item(row, col) is not None:
                table.item(row, col).setText(str(item))
            else:
                table.cellWidget(row, col).setText(str(item))

    def delete_row_table(self, table, row=None, row_id=None):
        """
        supprime une ligne dans un tableau
        """
        if row is None:
            if row_id not in self._ids2rows[table.objectName()]:
                return
            row = self._ids2rows[table.objectName()][row_id]
            del self._ids2rows[table.objectName()][row_id]
            for current_id in self._ids2rows[table.objectName()].keys():
                if self._ids2rows[table.objectName()][current_id] > row:
                    self._ids2rows[table.objectName()][current_id] -= 1
        table.removeRow(row)
