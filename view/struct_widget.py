from PyQt6.QtWidgets import QWidget
from PyQt6 import uic

import pathlib
ui_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'ui')
struct_widget_file = pathlib.Path.joinpath(ui_dir, 'struct_widget.ui')


class StructWidget(QWidget):
    def __init__(self, structure_hal, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        uic.loadUi(struct_widget_file, self)
        self._structure_hal = structure_hal

        self.docid_label.setText(self.get_value('docid'))
        self.acronym_label.setText(self.get_value('acronym_s'))
        self.label_label.setText(self.get_value('label_s'))
        self.valid_label.setText(self.get_value('valid_s'))
        self.type_label.setText(self.get_value('type_s'))
        self.adress_label.setText(self.get_value('address_s'))
        self.parent_label.setText(self.get_value('parentDocid_i'))
        url_string = '<a href="{url}">{url}<a>'.format(url = self.get_value('url_s'))
        self.url_label.setText(url_string)
        self.url_label.setOpenExternalLinks(True)
        self.end_date_label.setText(self.get_value('endDate_s'))
        self.start_date_label.setText(self.get_value('startDate_s'))

    def get_value(self, value_name:str) -> str:
        if type(self._structure_hal) is dict:
            if value_name in self._structure_hal:
                return str(self._structure_hal[value_name])
            else:
                return ''
        return str(getattr(self._structure_hal, value_name))
