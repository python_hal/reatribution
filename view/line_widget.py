from PyQt6.QtWidgets import QWidget, QMessageBox, QInputDialog
from PyQt6 import uic
from model.database import Element, Line, StructureHal
from view.element_widget import ElementWidget
from datetime import timedelta
from peewee import IntegrityError

import pathlib
ui_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'ui')
line_widget_file = pathlib.Path.joinpath(ui_dir, 'line_widget.ui')


class LineWidget(QWidget):
    """
    Widget pour l'affichage et l'édition d'une line
    """
    def __init__(self, line: Line, structure_hal: StructureHal=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        uic.loadUi(line_widget_file, self)
        if structure_hal is None:
            self.add_element_button.setParent(None)
        else:
            self.add_element_button.clicked.connect(self.add_element)

        self._line = line
        self._structure_hal = structure_hal
        self.label_line.setText(line.label)
        self.acronym_line.setText(line.acronym)
        self.struct_type_select.setCurrentText(line.type)
        self.refresh()

        self.save_label_button.clicked.connect(self.save_label)
        self.save_acronym_button.clicked.connect(self.save_acronym)
        self.save_type_button.clicked.connect(self.save_type)

        self.label_line.textChanged.connect(lambda :self.save_label_button.setEnabled(True))
        self.acronym_line.textChanged.connect(lambda :self.save_acronym_button.setEnabled(True))
        self.struct_type_select.currentTextChanged.connect(lambda :self.save_type_button.setEnabled(True))

    def save_label(self):
        """
        Enregistrer les modification du label
        """
        self._line.label = self.label_line.text()
        self._line.save()
        self.save_label_button.setEnabled(False)

    def save_acronym(self):
        """
        Enregistrer les modification du l'acronyme
        """
        self._line.acronym = self.acronym_line.text()
        self._line.save()
        self.save_acronym_button.setEnabled(False)

    def save_type(self):
        """
        Enregistrer les modification du type
        """
        self._line.type = self.struct_type_select.currentText()
        self._line.save()
        self.save_type_button.setEnabled(False)

    def add_element(self):
        """
        Ajouter un élément à la ligne
        """
        response = QMessageBox.question(self, 'Ajout à une nouvelle ligne', 'Êtes-vous sure de vouloire Ajoutez un élément à cette ligne ?')
        if response == QMessageBox.StandardButton.No.value:
            return
        date_start = self._structure_hal.startDate_s
        date_end = self._structure_hal.endDate_s
        hal_id = self._structure_hal.docid
        new_element = Element.get_or_none(Element.hal_id == hal_id)
        if new_element is None:
            new_element = Element(
                date_start = date_start,
                date_end = date_end,
                hal_id = hal_id
            )
            new_element.save()
        last_element = self._line.last_element
        try:
            self._line.elements.add(new_element)
            self._line.save()
            if new_element.date_start is not None and last_element.date_end is None:
                if last_element.date_start is None or last_element.date_start < new_element.date_start - timedelta(days=1):
                    last_element.date_end = new_element.date_start - timedelta(days=1)
                    last_element.save()
            if self._structure_hal.valid_s == 'VALID':
                if self._line.acronym != self._structure_hal.acronym_s:
                    new_acro, ok = QInputDialog.getText(self,
                                                        'Renommer l\'acronyme de la ligne',
                                                        'L\'élément ajouter n\'a pas le même acronyme que la ligne.',
                                                        text=self._structure_hal.acronym_s
                                                        )
                    if ok:
                        self._line.acronym = new_acro
                        self._line.save()
                        new_label, ok = QInputDialog.getText(self,
                                                             'Renommer l\'acronyme de la ligne',
                                                             'L\'élément ajouter n\'a pas le même acronyme que la ligne.',
                                                             text=self._structure_hal.label_s
                                                             )
                        if ok:
                            self._line.label = new_label
                            self._line.save()
            self.refresh()
        except IntegrityError:
            QMessageBox.warning(self, "Elément déjà présent", "L'élément que vous essayer d'ajouter est déjà présent dans la ligne.")

    def refresh(self):
        """
        Raffréchissement liste des element
        """
        layout = self.element_list_widget.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)
        elements = [element for element in self._line.elements.order_by(Element.date_start, -Element.date_end)]
        for element in elements:
            new_element_widget = ElementWidget(element, self._line)
            layout.addWidget(new_element_widget)
            new_element_widget.modified.connect(self.refresh)
