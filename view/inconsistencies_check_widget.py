from PyQt6.QtWidgets import QWidget, QListWidgetItem
from PyQt6 import uic
from model.database import *
from datetime import timedelta
from view.line_widget import LineWidget

import pathlib
ui_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'ui')
inconsistencies_check_widget_file = pathlib.Path.joinpath(ui_dir, 'inconsistencies_check_widget.ui')


def check_or_create(start_hal_id, end_hal_id, line):
    date_elements_inconsistencies = DateElementsInconsistencies.get_or_none(
        (DateElementsInconsistencies.start_hal_id == start_hal_id) &
        (DateElementsInconsistencies.end_hal_id == end_hal_id) &
        (DateElementsInconsistencies.line == line)
    )
    if date_elements_inconsistencies is None:
        date_elements_inconsistencies = DateElementsInconsistencies(start_hal_id=start_hal_id, end_hal_id=end_hal_id, line=line)
        date_elements_inconsistencies.save()
    return date_elements_inconsistencies


class InconsistenciesCheckWidget(QWidget):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        uic.loadUi(inconsistencies_check_widget_file, self)
        self._line_row = {}

        self.load_inconsistencies()

        # connect event
        self.check_inconsistencies_button.clicked.connect(self.check)
        self.inconsistencies_list.currentRowChanged.connect(self.inconsistencies_list_change)
        self.ignore_inconsistencies_check.stateChanged.connect(self.ignore_inconsistency)
        self.show_ignre_check.stateChanged.connect(self.load_inconsistencies)

    def load_inconsistencies(self):
        self._line_row = {}
        self.inconsistencies_list.clear()
        for incon in DateElementsInconsistencies.select():
            if not incon.ignore or self.show_ignre_check.isChecked():
                self.add_inconsistency(incon)

    def ignore_inconsistency(self):
        if self.inconsistencies_list.currentItem() is not None:
            incons = self.inconsistencies_list.currentItem().data(0x0100)
            for incon in incons:
                incon.ignore = self.ignore_inconsistencies_check.isChecked()
                incon.save()

    def inconsistencies_list_change(self):
        layout = self.line_widget.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)
        if self.inconsistencies_list.currentItem() is not None:
            incons = self.inconsistencies_list.currentItem().data(0x0100)
            selected_line = incons[0].line
            layout.addWidget(LineWidget(selected_line))
            self.ignore_inconsistencies_check.setChecked(incons[0].ignore)

    def add_inconsistency(self, incon):
        if incon.line.id not in self._line_row.keys():
            new_item = QListWidgetItem(str(incon.line.id))
            new_item.setData(0x0100, [])
            self._line_row[incon.line.id] = self.inconsistencies_list.count()
            self.inconsistencies_list.addItem(new_item)
        row = self._line_row[incon.line.id]
        row_data = self.inconsistencies_list.item(row).data(0x0100)
        if incon not in row_data:
            row_data.append(incon)
        self.inconsistencies_list.item(row).setData(0x0100, row_data)

    def check(self):
        """
        Verification de la cohérence des date pour chaque ligne
        """
        for line in Line.select():
            elements = [element for element in line.elements.order_by(Element.date_start, -Element.date_end)]
            for el_index, element in enumerate(elements):
                # ignorer le premier élémnent
                if el_index == 0:
                    continue
                prev_date = elements[el_index-1].date_end
                next_date = elements[el_index+1].date_start if el_index < len(elements) - 1 else None
                dates = (prev_date, next_date)
                checked = False
                if dates[1] is None or element.date_end == dates[1] - timedelta(days=1):
                    checked = True
                if checked and (dates[0] is not None and element.date_start != dates[0] + timedelta(days=1)):
                    checked = False
                if not checked or dates == (None, None):
                    incon = check_or_create(elements[el_index-1].hal_id, element.hal_id, line)
                    if not incon.ignore or self.show_ignre_check.isChecked():
                        self.add_inconsistency(incon)
                else:
                    inco_existant = DateElementsInconsistencies.get_or_none(
                        (DateElementsInconsistencies.start_hal_id == elements[el_index-1].hal_id) &
                        (DateElementsInconsistencies.end_hal_id == element.hal_id) &
                        (DateElementsInconsistencies.line == line)
                    )
                    if inco_existant is not None:
                        inco_existant.delete_instance()

