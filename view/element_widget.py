from PyQt6.QtWidgets import QWidget, QMessageBox
from PyQt6 import uic
from model.database import Element, StructureHal, ElementDateChange, db_change_notifier, Line, Error
from view.history_element_widget import HistoryElementWidget
from PyQt6.QtCore import pyqtSignal
import webbrowser
import requests
import datetime

import pathlib
ui_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'ui')
element_widget_file = pathlib.Path.joinpath(ui_dir, 'element_widget.ui')


class ElementWidget(QWidget):
    """ widget d'affichage et de manipulation de element
    """
    modified = pyqtSignal()

    def __init__(self, element: Element, line: Line=None, structure_hal: StructureHal=None, *args, **kwargs):
        """
        dates : (date de fin element précédent, date début élément suivant)
        """
        super().__init__(*args, **kwargs)
        uic.loadUi(element_widget_file, self)
        self._element = element
        self._structure_hal = structure_hal
        self._line = line
        self._line_element = None
        self._previous_element_line = None

        self.add_struct_button.setVisible(False)
        self.warning_structure_label.setVisible(False)
        if structure_hal is None:
            structure_hal = StructureHal.get_or_none(StructureHal.docid == element.hal_id)
        if structure_hal is None:
            self.warning_structure_label.setVisible(True)
            self.add_struct_button.setVisible(True)
            self.add_struct_button.clicked.connect(self.add_struct)
            for error in Error.select_from_instance(element):
                if error.error_valut == 0:
                    self.warning_structure_label.setText('structure non trouver')
        else:
            label = structure_hal.label_s if structure_hal.label_s is not None else ''
            acronym = structure_hal.acronym_s if structure_hal.acronym_s is not None else ''
            self.struct_name_label.setText('Nom structure AuréHal : ' + label + ' (' + acronym +')')

        # connexion signeaux
        self.modify_button.clicked.connect(lambda :self.set_modifiable())
        self.save_button.clicked.connect(self.save)
        self.historic_button.clicked.connect(self.show_historic)
        self.remove_from_line_button.clicked.connect(self.remove_from_line)
        self.correct_date_button.clicked.connect(self.correct_date)
        self.open_aurehal_button.clicked.connect(self.open_aurehal)

        # correction affichage selon les cas

        if line is None:
            self.remove_from_line_button.setParent(None)

        if structure_hal is not None:
            if element.date_start == structure_hal.startDate_s and element.date_end == structure_hal.endDate_s:
                self.correct_date_button.setVisible(False)
        else:
            self.correct_date_button.setVisible(False)
        self.hal_id_edit.setText(str(self._element.hal_id))

        db_change_notifier.add_all_events(self)
        self.db_update_element(element)
        if element.id is not None:
            self.set_modifiable(False)

    def add_struct(self):
        docid = self._element.hal_id
        structure_hal = StructureHal.get_or_none(StructureHal.docid == docid)
        if structure_hal is None:
            query_url = "https://api.archives-ouvertes.fr/ref/structure/"
            params = {
                'q': 'docid:' + str(docid),
                'fl': 'docid,acronym_s,label_s,valid_s,type_s,address_s,url_s,endDate_s,startDate_s,parentDocid_i',
            }
            now = datetime.datetime.now()
            response = requests.get(query_url, params)
            last_update = now + response.elapsed
            json_resp = response.json()
            if 'response' in json_resp and 'docs' in json_resp['response'] and len(json_resp['response']['docs']) >= 1:
                element_doc = json_resp['response']['docs'][0]
                structure_hal = StructureHal(
                    docid = docid,
                    last_update = last_update,
                    acronym_s = element_doc['acronym_s'] if 'acronym_s' in element_doc.keys() else None,
                    label_s = element_doc['label_s'] if 'label_s' in element_doc.keys() else None,
                    valid_s = element_doc['valid_s'] if 'valid_s' in element_doc.keys() else None,
                    type_s = element_doc['type_s'] if 'type_s' in element_doc.keys() else None,
                    address_s = element_doc['address_s'] if 'address_s' in element_doc.keys() else None,
                    url_s = element_doc['url_s'] if 'url_s' in element_doc.keys() else None,
                    endDate_s = element_doc['endDate_s'][0] if 'endDate_s' in element_doc.keys() else None,
                    startDate_s = element_doc['startDate_s'][0] if 'startDate_s' in element_doc.keys() else None,
                    parentDocid_i = ','.join(element_doc['parentDocid_i']) if 'parentDocid_i' in element_doc.keys() else None,
                )
                structure_hal.save()
            elif 'response' in json_resp and 'numFound' in json_resp['response'] and json_resp['response']['numFound'] == 0:
                error = Error(error_valut=0)
                error.object_instance = self._element
                error.save()
            self.modified.emit()

    def open_aurehal(self):
        webbrowser.open('https://aurehal.archives-ouvertes.fr/structure/read/id/' + str(self._element.hal_id))

    def remove_from_line(self):
        """
        Enlever de la ligne courante
        """
        self._line.elements.remove(self._element)
        self._line.save()
        if len(self._element.lines) == 0:
            self._element.delete_instance()
            db_change_notifier.emit_delete('Element', self._element.id)
        self.modified.emit()

    def show_historic(self):
        """
        Montrer l'historique des date de début et de fin
        """
        HistoryElementWidget(self._element).show()

    def db_update_element(self, element: Element):
        """
        événement mise à jour d'un élément
        """
        if self._element.id == element.id:
            self._element = element
            if not ElementDateChange.select().where(ElementDateChange.element == element).exists():
                self.historic_button.setVisible(False)
            else:
                self.historic_button.setVisible(True)
            if self._element.date_start is None:
                if self._element.unknow_start:
                    self.date_start_unknown_checkbox.setChecked(True)
                else:
                    self.date_start_null_checkbox.setChecked(True)
            else:
                self.date_start_edit.setDate(self._element.date_start)
            if self._element.date_end is None:
                if self._element.unknow_end:
                    self.date_end_unknown_checkbox.setChecked(True)
                else:
                    self.date_end_null_checkbox.setChecked(True)
            else:
                self.date_end_edit.setDate(self._element.date_end)

    def correct_date(self):
        """
        correction automatique des dates de début et de fin.
        """
        if self._structure_hal.endDate_s is not None or self._line is None:
            self._element.date_end = self._structure_hal.endDate_s
        if self._structure_hal.startDate_s is not None or self._line is None:
            self._element.date_start = self._structure_hal.startDate_s
        self._element.save()
        self.modified.emit()

    def set_modifiable(self, modifiable=True):
        """
        rendre modifiable ou non modifiable
        """
        if modifiable:
            self.modify_button.setEnabled(False)
            self.save_button.setEnabled(True)
            self.date_start_edit.setEnabled(True)
            self.date_start_null_checkbox.setEnabled(True)
            self.date_start_unknown_checkbox.setEnabled(True)
            self.date_end_edit.setEnabled(True)
            self.date_end_null_checkbox.setEnabled(True)
            self.date_end_unknown_checkbox.setEnabled(True)
            self.hal_id_edit.setEnabled(True)
        else:
            self.modify_button.setEnabled(True)
            self.save_button.setEnabled(False)
            self.date_start_edit.setEnabled(False)
            self.date_start_null_checkbox.setEnabled(False)
            self.date_start_unknown_checkbox.setEnabled(False)
            self.date_end_edit.setEnabled(False)
            self.date_end_null_checkbox.setEnabled(False)
            self.date_end_unknown_checkbox.setEnabled(False)
            self.hal_id_edit.setEnabled(False)


    def save(self):
        """
        Enregistrer les modification
        """
        if self.date_start_null_checkbox.isChecked():
            self._element.date_start = None
        elif self.date_start_unknown_checkbox.isChecked():
            self._element.date_start = None
            self._element.unknow_start = True
        else:
            self._element.date_start = self.date_start_edit.date().toPyDate()
        if self.date_end_null_checkbox.isChecked():
            self._element.date_end = None
        elif self.date_end_unknown_checkbox.isChecked():
            self._element.date_end = None
            self._element.unknow_end = True
        else:
            self._element.date_end = self.date_end_edit.date().toPyDate()
        try:
            self._element.hal_id = int(self.hal_id_edit.text())
            if self._element.id is None and self._line is not None:
                self._element.save()
                self._line.elements.add(self._element)
                self._line.save()
            else:
                self._element.save()
            self.modified.emit()
            self.set_modifiable(False)
        except ValueError:
            QMessageBox.about(self, "erreur hal_id", "hal_id doit être un entier.")
