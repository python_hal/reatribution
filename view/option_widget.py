# import PyQt6
from PyQt6.QtWidgets import QWidget
from PyQt6 import uic

# import HalTo
from controler.controler import Controler, BachupDB
from model.option import Options, AutoBackupOption

# import Python
import os
import glob

# import and use Keyring
import keyring
from keyring.backends import Windows
keyring.set_keyring(Windows.WinVaultKeyring())

# access paths
import pathlib
current_dir = pathlib.Path(__file__).parent.resolve()
ui_dir = current_dir / 'ui'
option_widge_file = pathlib.Path.joinpath(ui_dir, 'option_widget.ui')


class OptionWidget(QWidget):
    """ Fenétre des option
    """
    def __init__(self, option: Options, controler: Controler, *args, **kwargs):
        super().__init__(*args, **kwargs)
        uic.loadUi(option_widge_file, self)
        self._option = option

        def make_error():
            raise TypeError("message de test")

        self.make_bug_button.clicked.connect(make_error)

        self.start_year_spin.setValue(int(self._option.start_year))

        self.save_button.clicked.connect(self.save)
        self.cancel_button.clicked.connect(self.close)
        self.del_backups_button.clicked.connect(self.del_backups)
        self.instance_combo.currentIndexChanged.connect(self.instance_change)
        self.auto_backup_combo.currentIndexChanged.connect(self.auto_backup_change)
        self.clean_log_button.clicked.connect(self.clean_log)

        self.backup_db_list.currentRowChanged.connect(lambda x: self.del_backup_button.setEnabled(x!=-1))
        self.del_backup_button.clicked.connect(self.del_backup)

        self._controler = controler
        for backup in controler.db_backups:
            self.backup_db_list.addItem(str(backup))

        current_backup_option = self._option.autobackup
        for backup_option in AutoBackupOption:
            self.auto_backup_combo.addItem(backup_option.fr_label, backup_option.value)
            if current_backup_option == backup_option:
                self.auto_backup_combo.setCurrentIndex(self.auto_backup_combo.count()-1)

        login = self._option.login
        self.login_lineedit.setText(login)
        password = keyring.get_password("halto", login)
        if password is not None:
            self.password_lineedit.setText(password)
            self.del_password_button.setEnabled(True)
            self.del_password_button.clicked.connect(self.del_password)

        current_instance_id = self._option.instance.id if self._option.instance is not None else -1
        for instance in self._option.instances.instances_sorted:
            self.instance_combo.addItem(instance.name, instance.id)
            if current_instance_id == instance.id:
                self.instance_combo.setCurrentIndex(self.instance_combo.count()-1)

    def del_backup(self):
        BachupDB(self.backup_db_list.currentItem().text()).delete()
        self.backup_db_list.clear()
        for backup in self._controler.db_backups:
            self.backup_db_list.addItem(str(backup))
        self.del_backup_button.setEnabled(False)

    def clean_log(self):
        with open(current_dir.parent / 'error.log', 'w'): pass

    def auto_backup_change(self):
        if self.auto_backup_combo.currentIndex() != -1:
            self._option.autobackup = self.auto_backup_combo.currentData()

    def instance_change(self):
        if self.instance_combo.currentIndex() != -1:
            self._option.instance = self.instance_combo.currentData()

    def del_backups(self):
        backup_dir = str(pathlib.Path(__file__).parent.parent / 'backup')
        files = glob.glob(backup_dir + '/*')
        for file in files:
            os.remove(file)

    def del_password(self):
        login = self.login_lineedit.text()
        self.password_lineedit.setText('')
        if keyring.get_password("halto", login) is not None:
            keyring.delete_password("halto", login)
        self.del_password_button.setEnabled(False)

    def save(self):
        self._option.start_year= str(self.start_year_spin.value())
        login = self.login_lineedit.text()
        self._option.login = login
        if self.password_lineedit.text() != '':
            keyring.set_password("halto", login, self.password_lineedit.text())
        self._option.save()
        self.close()
