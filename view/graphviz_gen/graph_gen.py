import pydot
from model.database import *
from abc import ABC, abstractmethod
from typing import Annotated

# Ajout graphviz binary to path
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(os.path.dirname(os.path.dirname(dir_path)))
path = os.path.join(os.path.join(os.path.join(dir_path, 'Graphviz')), 'bin')
os.environ["PATH"] += os.pathsep + path


class GraphGenerator(ABC):
    """ Class de base pour les générateur de graphique basé sur graphviz
    """
    def init_graph(self):
        base = """digraph reatribution {node [shape = record];rankdir=LR;}"""
        graphs = pydot.graph_from_dot_data(base)
        self._graph = graphs[0]

    @abstractmethod
    def generate_graph():
        pass

    def save_img(self, file_name: str, format: str = 'svg') -> None:
        self.generate_graph()
        if format == 'svg':
            self._graph.write_svg(file_name)
        elif format == 'png':
            self._graph.write_png(file_name)
        elif format == 'jpg':
            self._graph.write_jpg(file_name)
        elif format == 'dot':
            self._graph.write_raw(file_name)


class HalIdNode(object):
    def __init__(self, element: Element=None, form_hal_id: int=-1) -> None:
        if form_hal_id != -1:
            element = Element.get_or_none(Element.hal_id == form_hal_id)
        else:
            form_hal_id = element.hal_id
        if element is None:
            self._label = 'Inconnu|' + str(form_hal_id)
        else:
            self._label = '{'
            for i, line in enumerate(element.lines):
                if i != 0:
                    self._label += "|"
                self._label += line.label.replace('->', ':')
            self._label += '}|'
            self._label += str(form_hal_id)

    def __str__(self) -> str:
        return self._label


class HalIdNodeColection(object):
    def __init__(self, nodes:dict = None) -> None:
        if nodes is None:
            self._nodes = {}
        else:
            self._nodes = nodes

    def __getitem__(self, key):
        if key not in self._nodes.keys():
            self._nodes[key] = HalIdNode(form_hal_id = key)
        return self._nodes[key]
    
    def __setitem__(self, key, value):
        self._nodes[key] = value

    def items(self):
        return self._nodes.items()


class StructGraph(GraphGenerator):
    def __init__(self, line: Line) -> None:
        self._edges = []
        self._nodes = {}
        self._lines = [line]
        for element in line.elements:
            self.add_element(element)

    def add_edge(self, start_element, end_element):
        start_id = start_element.hal_id
        end_id = end_element.hal_id
        if (start_id, end_id) not in self._edges:
            self._edges.append((start_id, end_id))

    def add_node_element(self, element):
        if element.hal_id not in self._nodes.keys():
            self._nodes[element.hal_id] = HalIdNode(element)


    def add_element(self, element: Element) -> None:
        for line in element.lines:
            self._lines.append(line)
            elements = [element for element in self._line.elements.order_by(Element.date_start, -Element.date_end)]
            for el_index, element in enumerate(elements):
                self.add_node_element(element)
                if el_index >= 0:
                    previous_element = elements[el_index-1]
                    self.add_node_element(previous_element)
                    self.add_edge(previous_element, element)
                if el_index < len(elements) - 1:
                    next_element = elements[el_index+1]
                    self.add_node_element(next_element)
                    self.add_edge(element, next_element)

    def generate_graph(self):
        self.init_graph()
        for halid, node in self._nodes.items():
            element_node = pydot.Node(halid, shape='record', label=str(node))
            self._graph.add_node(element_node)
        self.add_edge_to_graph()

    def add_edge_to_graph(self):
        for edge in self._edges:
            self._graph.add_edge(pydot.Edge(edge[0], edge[1]))


    @property
    def lines(self):
        return self._lines


class DatedStructbGraph(StructGraph):

    def add_edge(self, start_element, end_element):
        start_id = start_element.hal_id
        end_id = end_element.hal_id
        edge = (start_id, end_id, date_to_string(start_element.date_end, True))
        if edge not in self._edges:
            self._edges.append(edge)

    def add_edge_to_graph(self):
        for edge in self._edges:
            self._graph.add_edge(pydot.Edge(edge[0], edge[1], label=edge[2]))


class StructbWithReatrib(DatedStructbGraph):
    def __init__(self, line: Line) -> None:
        self._line = line
        self._reatribution = []
        super().__init__(line)
        self._nodes = HalIdNodeColection(self._nodes)
    
    def init_graph(self):
        self._mega_graph = pydot.Dot(graph_type='digraph', rankdir='LR')
        self._graph = pydot.Subgraph('')
        self._mega_graph.add_subgraph(self._graph)
        
    def add_node_element(self, element):
        super().add_node_element(element)
        for reatribution in Reatribution.select().where((Reatribution.start_element == element.hal_id) | (Reatribution.end_element == element.hal_id)):
            if reatribution not in self._reatribution:
                self._reatribution.append(reatribution)

    def generate_graph(self):
        super().generate_graph()
        reatrib_graph = pydot.Cluster('reatributions', label='reatributions', style='filled', fillcolor='white')
        prefix = 'reatribution'
        node_added = []
        edge_added = []
        for reatribution in self._reatribution:
            if reatribution.start_element not in node_added:
                start_element_node = pydot.Node(prefix + str(reatribution.start_element), shape='record', label=str(self._nodes[reatribution.start_element]))
                reatrib_graph.add_node(start_element_node)
                node_added.append(reatribution.start_element)
            if reatribution.end_element not in node_added:
                end_element_node = pydot.Node(prefix + str(reatribution.end_element), shape='record', label=str(self._nodes[reatribution.end_element]))
                reatrib_graph.add_node(end_element_node)
                node_added.append(reatribution.end_element)
            if reatribution.period not in node_added:
                periode_node = pydot.Node(prefix + reatribution.period, label=reatribution.period, shape='oval')
                reatrib_graph.add_node(periode_node)
                node_added.append(reatribution.period)
            reatrib_graph.add_edge(pydot.Edge(prefix + str(reatribution.start_element), prefix + reatribution.period, color='red'))
            if (prefix + reatribution.period, prefix + str(reatribution.end_element)) not in edge_added:
                reatrib_graph.add_edge(pydot.Edge(prefix + reatribution.period, prefix + str(reatribution.end_element), color='green'))
                edge_added.append((prefix + reatribution.period, prefix + str(reatribution.end_element)))
        self._mega_graph.add_subgraph(reatrib_graph)
        self._graph = self._mega_graph


class ReatribtionGraph(GraphGenerator):
    def __init__(self, reatribution: Reatribution) -> None:
        self._reatributions = []
        self._hal_id = []
        self.add_reatribution(reatribution)
        self._nodes = HalIdNodeColection()

    @property
    def reatributions_ids(self):
        return [reatribution.id for reatribution in self._reatributions]

    def generate_graph(self):
        self.init_graph()
        node_added = []
        edge_added = []
        for reatribution in self._reatributions:
            if reatribution.start_element not in node_added:
                start_element_node = pydot.Node(str(reatribution.start_element), shape='record', label=str(self._nodes[reatribution.start_element]))
                self._graph.add_node(start_element_node)
                node_added.append(reatribution.start_element)
            if reatribution.end_element not in node_added:
                end_element_node = pydot.Node(str(reatribution.end_element), shape='record', label=str(self._nodes[reatribution.end_element]))
                self._graph.add_node(end_element_node)
                node_added.append(reatribution.end_element)
            if reatribution.period not in node_added:
                periode_node = pydot.Node(reatribution.period, label=reatribution.period, shape='oval')
                self._graph.add_node(periode_node)
                node_added.append(reatribution.period)
            self._graph.add_edge(pydot.Edge(str(reatribution.start_element), reatribution.period, color='red'))
            if (reatribution.period, str(reatribution.end_element)) not in edge_added:
                self._graph.add_edge(pydot.Edge(reatribution.period, str(reatribution.end_element), color='green'))
                edge_added.append((reatribution.period, str(reatribution.end_element)))

    def add_hal_ids(self, hal_id: int):
        if hal_id in self._hal_id:
            return
        self._hal_id.append(hal_id)
        for new_reatribution in self.get_reatribe_for_halid(hal_id):
            self.add_reatribution(new_reatribution)

    def add_reatribution(self, reatribution: Reatribution):
        self.add_hal_ids(reatribution.start_element)
        self.add_hal_ids(reatribution.end_element)
        if reatribution not in self._reatributions:
            self._reatributions.append(reatribution)

    def get_reatribe_for_halid(self, hal_id: int):
        return Reatribution.select().where(((Reatribution.start_element == hal_id) | (Reatribution.end_element == hal_id)))


class StructDiffOneGraph(DatedStructbGraph):
    def __init__(self, line: Line, colors: Annotated[list[str], 2] = ['gold', 'cornflowerblue']) -> None:
        self._colors = colors
        self._line = line
        self._reatribution = {}
        super().__init__(line)
        self._nodes = HalIdNodeColection(self._nodes)
        
    def add_node_element(self, element):
        if element.hal_id not in self._nodes.keys():
            sources = [reatrib.source.id for reatrib in Reatribution.select().where((Reatribution.start_element == element.hal_id) | (Reatribution.end_element == element.hal_id)) if reatrib.source.id in self._sources]
            self._nodes[element.hal_id] = (HalIdNode(element), list(set(sources)))
        for reatribution in Reatribution.select().where((Reatribution.start_element == element.hal_id) | (Reatribution.end_element == element.hal_id)):
            if reatribution.source.id not in self._sources:
                continue
            if (reatribution.start_element, reatribution.end_element, reatribution.period) not in self._reatribution.keys():
                self._reatribution[(reatribution.start_element, reatribution.end_element, reatribution.period)] = [reatribution.source.id]
            elif reatribution.source.id not in self._reatribution[(reatribution.start_element, reatribution.end_element, reatribution.period)]:
                self._reatribution[(reatribution.start_element, reatribution.end_element, reatribution.period)].append(reatribution.source.id)

    def generate_graph(self):
        self._graph = pydot.Dot(graph_type='digraph', rankdir='LR')
        sources = [Source.get_by_id(source_id).title for source_id in self._sources]
        legend = pydot.Cluster('legend', label='legend', style='filled', fillcolor='white')
        for idx in (0, 1):
            legend.add_node(pydot.Node('s' + str(idx) + 'a', style='invis'))
            legend.add_node(pydot.Node('s' + str(idx) + 'b', shape='record', label='{source|' + sources[idx] + '}|1041636', style='filled', fillcolor=self._colors[idx]))
            legend.add_edge(pydot.Edge(
                's' + str(idx) + 'a',
                's' + str(idx) + 'b',
                label=sources[idx],
                color=self._colors[idx],
                style='dashed'
            ))
        legend.add_node(pydot.Node('node_none', shape='record', label='{source|autre line}|1041636', style='filled', fillcolor='lightgray'))
        self._graph.add_subgraph(legend)
        node_added = []
        for halid, node in self._nodes.items():
            color = 'white'
            if len(node[1]) == 1:
                color = self._colors[self._sources.index(node[1][0])]
            element_node = pydot.Node(halid, shape='record', label=str(node[0]), style='filled', fillcolor=color)
            self._graph.add_node(element_node)
            node_added.append(halid)
        green_edges = {}
        edge_added = []
        for (start_element, end_element, period), reatrib_sources in self._reatribution.items():
            for element_id in [start_element, end_element]:
                if element_id not in node_added:
                    element_node = pydot.Node(element_id, shape='record', label=str(self._nodes[element_id]), style='filled', fillcolor='lightgray')
                    self._graph.add_node(element_node)
                    node_added.append(element_id)
            if period not in node_added:
                periode_node = pydot.Node(period, label=period, shape='oval')
                self._graph.add_node(periode_node)
                node_added.append(period)
            if (period, end_element) not in green_edges.keys():
                green_edges[(period, end_element)] = reatrib_sources
            if len(green_edges[(period, end_element)]) != 1 or len(reatrib_sources) != 1 or green_edges[(period, end_element)][0] != reatrib_sources[0]:
                if (period, end_element) not in edge_added:
                    self._graph.add_edge(pydot.Edge(period, str(end_element), color='green'))
                    edge_added.append((period, end_element))
            if len(reatrib_sources) == 1:
                color = self._colors[self._sources.index(reatrib_sources[0])]
                self._graph.add_edge(pydot.Edge(start_element, period, color=color, style='dashed'))
            else:
                self._graph.add_edge(pydot.Edge(start_element, period, color='red'))
        for (period, end_element), edge_sources in green_edges.items():
            if (period, end_element) not in edge_added:
                if len(edge_sources) == 1:
                    self._graph.add_edge(pydot.Edge(period, str(end_element), color=self._colors[self._sources.index(edge_sources[0])], style='dashed'))
                else:
                    self._graph.add_edge(pydot.Edge(period, str(end_element), color='green'))
