# Form implementation generated from reading ui file 'c:\Users\admin-gpicot\Documents\reattribhal\view\ui\line_widget.ui'
#
# Created by: PyQt6 UI code generator 6.4.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic6 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt6 import QtCore, QtGui, QtWidgets


class Ui_line_widget(object):
    def setupUi(self, line_widget):
        line_widget.setObjectName("line_widget")
        line_widget.resize(378, 365)
        self.verticalLayout = QtWidgets.QVBoxLayout(line_widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget_2 = QtWidgets.QWidget(parent=line_widget)
        self.widget_2.setObjectName("widget_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.widget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(parent=self.widget_2)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.label_line = QtWidgets.QLineEdit(parent=self.widget_2)
        self.label_line.setObjectName("label_line")
        self.horizontalLayout_2.addWidget(self.label_line)
        self.save_label_button = QtWidgets.QPushButton(parent=self.widget_2)
        self.save_label_button.setEnabled(False)
        self.save_label_button.setObjectName("save_label_button")
        self.horizontalLayout_2.addWidget(self.save_label_button)
        self.verticalLayout.addWidget(self.widget_2)
        self.widget_3 = QtWidgets.QWidget(parent=line_widget)
        self.widget_3.setObjectName("widget_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.widget_3)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_5 = QtWidgets.QLabel(parent=self.widget_3)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_3.addWidget(self.label_5)
        self.acronym_line = QtWidgets.QLineEdit(parent=self.widget_3)
        self.acronym_line.setObjectName("acronym_line")
        self.horizontalLayout_3.addWidget(self.acronym_line)
        self.save_acronym_button = QtWidgets.QPushButton(parent=self.widget_3)
        self.save_acronym_button.setEnabled(False)
        self.save_acronym_button.setObjectName("save_acronym_button")
        self.horizontalLayout_3.addWidget(self.save_acronym_button)
        self.verticalLayout.addWidget(self.widget_3)
        self.widget_4 = QtWidgets.QWidget(parent=line_widget)
        self.widget_4.setObjectName("widget_4")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.widget_4)
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_7 = QtWidgets.QLabel(parent=self.widget_4)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_4.addWidget(self.label_7)
        self.struct_type_select = QtWidgets.QComboBox(parent=self.widget_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Expanding, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.struct_type_select.sizePolicy().hasHeightForWidth())
        self.struct_type_select.setSizePolicy(sizePolicy)
        self.struct_type_select.setEditable(False)
        self.struct_type_select.setObjectName("struct_type_select")
        self.struct_type_select.addItem("")
        self.struct_type_select.addItem("")
        self.struct_type_select.addItem("")
        self.struct_type_select.addItem("")
        self.struct_type_select.addItem("")
        self.horizontalLayout_4.addWidget(self.struct_type_select)
        self.save_type_button = QtWidgets.QPushButton(parent=self.widget_4)
        self.save_type_button.setEnabled(False)
        self.save_type_button.setObjectName("save_type_button")
        self.horizontalLayout_4.addWidget(self.save_type_button)
        self.verticalLayout.addWidget(self.widget_4)
        self.show_element_checkbox = QtWidgets.QCheckBox(parent=line_widget)
        self.show_element_checkbox.setChecked(True)
        self.show_element_checkbox.setObjectName("show_element_checkbox")
        self.verticalLayout.addWidget(self.show_element_checkbox)
        self.element_scrollarea = QtWidgets.QScrollArea(parent=line_widget)
        self.element_scrollarea.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.element_scrollarea.setLineWidth(0)
        self.element_scrollarea.setWidgetResizable(True)
        self.element_scrollarea.setObjectName("element_scrollarea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 378, 198))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.element_list_widget = QtWidgets.QWidget(parent=self.scrollAreaWidgetContents)
        self.element_list_widget.setObjectName("element_list_widget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.element_list_widget)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout_2.addWidget(self.element_list_widget)
        spacerItem = QtWidgets.QSpacerItem(20, 84, QtWidgets.QSizePolicy.Policy.Minimum, QtWidgets.QSizePolicy.Policy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.element_scrollarea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout.addWidget(self.element_scrollarea)
        self.widget = QtWidgets.QWidget(parent=line_widget)
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem1 = QtWidgets.QSpacerItem(231, 20, QtWidgets.QSizePolicy.Policy.Expanding, QtWidgets.QSizePolicy.Policy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.add_element_button = QtWidgets.QPushButton(parent=self.widget)
        self.add_element_button.setObjectName("add_element_button")
        self.horizontalLayout.addWidget(self.add_element_button)
        self.verticalLayout.addWidget(self.widget)

        self.retranslateUi(line_widget)
        QtCore.QMetaObject.connectSlotsByName(line_widget)

    def retranslateUi(self, line_widget):
        _translate = QtCore.QCoreApplication.translate
        line_widget.setWindowTitle(_translate("line_widget", "Form"))
        self.label_2.setText(_translate("line_widget", "Label : "))
        self.save_label_button.setText(_translate("line_widget", "Enregistrer"))
        self.label_5.setText(_translate("line_widget", "Acronym : "))
        self.save_acronym_button.setText(_translate("line_widget", "Enregistrer"))
        self.label_7.setText(_translate("line_widget", "Type : "))
        self.struct_type_select.setItemText(0, _translate("line_widget", "laboratory"))
        self.struct_type_select.setItemText(1, _translate("line_widget", "institution"))
        self.struct_type_select.setItemText(2, _translate("line_widget", "researchteam"))
        self.struct_type_select.setItemText(3, _translate("line_widget", "department"))
        self.struct_type_select.setItemText(4, _translate("line_widget", "regrouplaboratory"))
        self.save_type_button.setText(_translate("line_widget", "Enregistrer"))
        self.show_element_checkbox.setText(_translate("line_widget", "afficher les elements"))
        self.add_element_button.setText(_translate("line_widget", "Ajouter element"))
