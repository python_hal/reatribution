from PyQt6.QtWidgets import QWidget, QListWidgetItem
from PyQt6 import uic
from model.database import *

import pathlib
ui_dir = pathlib.Path.joinpath(pathlib.Path(__file__).parent.resolve(), 'ui')
history_element_widget_file = pathlib.Path.joinpath(ui_dir, 'history_element_widget.ui')


class HistoryElementWidget(QWidget):
    def __init__(self, element:Element, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        uic.loadUi(history_element_widget_file, self)
        db_change_notifier.add_all_events(self)
        self._element = element
        self.db_update_element(element)

        self.start_history_list.currentRowChanged.connect(self.start_history_list_change)
        self.end_history_list.currentRowChanged.connect(self.end_history_list_change)

        self.restaure_start_button.clicked.connect(self.restaure_start)
        self.restaure_end_button.clicked.connect(self.restaure_end)

        for date_change in ElementDateChange.select().where(ElementDateChange.element == element):
            self.db_new_element_date_change(date_change)

    def restaure_end(self):
        curentItem = self.end_history_list.currentItem()
        if curentItem is not None:
            self._element.date_end = curentItem.data(0x0100)
            self._element.save()

    def restaure_start(self):
        curentItem = self.start_history_list.currentItem()
        if curentItem is not None:
            self._element.date_start = curentItem.data(0x0100)
            self._element.save()

    def end_history_list_change(self):
        if self.end_history_list.currentRow() != -1:
            self.restaure_end_button.setEnabled(True)
        else:
            self.restaure_end_button.setEnabled(False)

    def start_history_list_change(self):
        if self.start_history_list.currentRow() != -1:
            self.restaure_start_button.setEnabled(True)
        else:
            self.restaure_start_button.setEnabled(False)

    def db_new_element_date_change(self, new_element_date_change:ElementDateChange):
        if new_element_date_change.element == self._element:
            new_item = QListWidgetItem(str(new_element_date_change.original_date))
            new_item.setData(0x0100, new_element_date_change.original_date)
            if new_element_date_change.is_start:
                self.start_history_list.addItem(new_item)
            else:
                self.end_history_list.addItem(new_item)

    def db_update_element(self, element: Element):
        if self._element.id == element.id:
            self._element = element
            if element.date_start is None:
                if element.unknow_start:
                    self.date_start_unknown_checkbox.setChecked(True)
                else:
                    self.date_start_null_checkbox.setChecked(True)
            else:
                self.date_start_edit.setDate(element.date_start)
            if element.date_end is None:
                if element.unknow_end:
                    self.date_end_unknown_checkbox.setChecked(True)
                else:
                    self.date_end_null_checkbox.setChecked(True)
            else:
                self.date_end_edit.setDate(element.date_end)
